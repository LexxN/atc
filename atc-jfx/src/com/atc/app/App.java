package com.atc.app;

import java.util.ArrayList;
import java.util.List;

import com.atc.app.entity.Airport;
import com.atc.app.entity.User;
import com.atc.app.screenframework.ScreensController;

import javafx.application.Application;
import javafx.beans.binding.BooleanBinding;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class App extends Application {
	public static Stage primaryStage;
	public static String screen1ID = "login";
    public static String screen1File = "view/Login.fxml";
    public static String screen2ID = "main";
    public static String screen2File = "view/Main.fxml";
    public static User dispatcher;
    public static Airport airport;
    public static List<BooleanBinding> bindings = new ArrayList<BooleanBinding>();
    public static final Integer waypointHeight = 20;
    public static final boolean enableTesting = false;
    public static final Integer airplaneClearance = 20;
    
	@Override
    public void start(Stage primaryStage) {
		App.primaryStage = primaryStage;
        ScreensController mainContainer = new ScreensController();
        mainContainer.loadScreen(App.screen1ID, App.screen1File);
        mainContainer.loadScreen(App.screen2ID, App.screen2File);
        mainContainer.setScreen(App.screen1ID);
        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
	    Scene scene = new Scene(root, screenBounds.getWidth(), screenBounds.getHeight());
	    scene.getStylesheets().add("/styles/main.css");
        primaryStage.setTitle("Air Traffic Controller App");
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.setFullScreen(true);
        primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        primaryStage.show();
    }
	
	public static void main(String[] args) {
		launch(args);
	}
}
