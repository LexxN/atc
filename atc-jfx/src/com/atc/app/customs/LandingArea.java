package com.atc.app.customs;

import com.atc.app.App;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class LandingArea extends Polygon{
	
	private Integer landingAngle;
	private Color lineColor = Color.TRANSPARENT;

	public LandingArea(String runwayName, Integer landingAngle, Double[] points) {
		if(App.enableTesting) {
			lineColor = Color.RED;
		}
		this.setId(runwayName + String.valueOf(landingAngle));
		this.landingAngle = landingAngle;
		this.getPoints().addAll(points);
		this.setFill(Color.TRANSPARENT);
		this.setStroke(lineColor);
	}

	public Integer getLandingAngle() {
		return landingAngle;
	}
	
	public BooleanBinding assignBinding(LandingArea area, PlaneElement flight) {
		
		BooleanBinding intersects = new BooleanBinding() {
        	
            {
                this.bind(area.boundsInParentProperty(), flight.boundsInParentProperty());
            }

            @Override
            protected boolean computeValue() {
                return area.getBoundsInParent().intersects(flight.getBoundsInParent());
            }

        };
        
        intersects.addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    flight.runwayApproach.add(area.getId());
                }
                else {
                	flight.runwayApproach.remove(area.getId());
                }
            }

        });
        
		return intersects;
	}
}
