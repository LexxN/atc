package com.atc.app.customs;

import com.atc.app.controller.FlightController;

import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

public class TurnPath extends Path {
	private static double arcRadius = 30;
	private static final double degreeOffcet = 180;
	private static final double EPSILON = 0.00001;
	private static double tempX = 0;
	private static double tempY = 0;
	private static boolean changeCoords;
	
	static class SmallCurve {
		public double x1;
		public double y1;
		public double x2;
		public double y2;
		public double x3;
		public double y3;
		public double x4;
		public double y4;
		
		public SmallCurve(double x1, double y1, double x2, double y2, double x3,
				double y3, double x4, double y4) {
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
			this.x3 = x3;
			this.y3 = y3;
			this.x4 = x4;
			this.y4 = y4;
		}
	}
	
	/*
	 * Calculates turn flight paths
	 */
	public TurnPath(Integer currentBearing, Integer newBearing, Integer angle, String turn, double startX, double startY) {
		boolean turnType;
		changeCoords = false;
		double startAngle, endAngle;
		
		if((currentBearing < newBearing && turn.equals("noturn")) || turn.equals("r")) {
			startAngle = currentBearing;
			endAngle = startAngle + angle;
			if((startAngle < 180 && endAngle <= 270)) {
				turnType = false;
			}
			else {
				turnType = true;
			}
		}
		else {
			startAngle = currentBearing + 180;
			endAngle = startAngle - angle;
			if((startAngle <= 360)) {
				turnType = true;
			}
			else {
				turnType = false;
			}
		}
		startAngle = (startAngle - degreeOffcet)*(Math.PI / 180);
        endAngle = (endAngle - degreeOffcet)*(Math.PI / 180);
        
		final double twoPI = Math.PI * 2;
		startAngle = startAngle % twoPI;
		endAngle = endAngle % twoPI;
        
        final double piOverTwo = Math.PI / 2.0;
        final int sgn = (startAngle < endAngle) ? 1 : -1;
		
        double a1 = startAngle;
        boolean flag = false;
        for (double totalAngle = Math.min(twoPI, Math.abs(endAngle - startAngle)); totalAngle > EPSILON; ) { 
            double a2 = a1 + sgn * Math.min(totalAngle, piOverTwo);
            TurnPath.SmallCurve smallCurve = createSmallArc(a1, a2, startX, startY, turnType);
            if(!flag) {
            	this.getElements().add(new MoveTo(smallCurve.x1, smallCurve.y1));
            	flag = true;
        	}
            if(smallCurve.x4 < 0 || smallCurve.x4 > FlightController.getMaxXCoord() || smallCurve.y4 < 0 || smallCurve.y4 > FlightController.getMaxYCoord()) {
   			 	break;
   		 	}
            else {
            	CubicCurveTo smallArc = createCubicCurve(smallCurve);
                this.getElements().add(smallArc);
                totalAngle -= Math.abs(a2 - a1);
                a1 = a2;
            }
            
        }
	}
	
	public static double getArcRadius() {
		return arcRadius;
	}
	
	private static TurnPath.SmallCurve createSmallArc(double a1, double a2, double startX, double startY, boolean turnType) {
		double a = (a2 - a1) / 2.0;
    	
    	double x4 = arcRadius * Math.cos(a);
    	double y4 = arcRadius * Math.sin(a);
    	double x1 = x4;
    	double y1 = -y4;
    	
    	final double k = 0.5522847498;
        final double f = k * Math.tan(a);
        
    	double x2 = x1 + f * y4;
    	double y2 = y1 + f * x4;
    	double x3 = x2; 
    	double y3 = -y2;

        final double ar = a + a1;
    	final double cos_ar = Math.cos(ar);
    	final double sin_ar = Math.sin(ar);
    	
    	if((startX != (startX + arcRadius * Math.cos(a1)) || startY != (startY + arcRadius * Math.sin(a1))) && !changeCoords) {
        	tempX = Math.abs(startX - startX + arcRadius * Math.cos(a1));
        	tempY = Math.abs(startY - startY + arcRadius * Math.sin(a1));
        	if(turnType) {
        		tempX *= -1;
        		tempY *= -1;
        	}
        	changeCoords = true;
        }
    	return new TurnPath.SmallCurve(tempX + startX + arcRadius * Math.cos(a1), tempY + startY + arcRadius * Math.sin(a1), 
    			tempX + startX + x2 * cos_ar - y2 * sin_ar, tempY + startY + x2 * sin_ar + y2 * cos_ar, 
    			tempX + startX + x3 * cos_ar - y3 * sin_ar, tempY + startY + x3 * sin_ar + y3 * cos_ar, 
    			tempX + startX + arcRadius * Math.cos(a2), tempY + startY + arcRadius * Math.sin(a2)
    		   );
	}
	
	 private static CubicCurveTo createCubicCurve(TurnPath.SmallCurve oCurve) {
		 CubicCurveTo cubicTo = new CubicCurveTo();
	     cubicTo.setControlX1(oCurve.x2);
	     cubicTo.setControlY1(oCurve.y2);
	     cubicTo.setControlX2(oCurve.x3);
	     cubicTo.setControlY2(oCurve.y3);
	     cubicTo.setX(oCurve.x4);
	     cubicTo.setY(oCurve.y4);
	    
	     return cubicTo;
	}

}
