package com.atc.app.customs;

import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

public class FlightPath extends Path {
	/*
	 * Calculates straight flight paths with current bearing as input parameter
	 */
	public FlightPath(double startX, double startY, double newBearing, PlaneElement oPlane) {
		double x2, y2, angle = Math.toRadians(newBearing - 90);
		x2 = startX + 2000 * Math.cos(angle);
		y2 = startY + 2000 * Math.sin(angle);
	    
	    LineTo lineTo = new LineTo(x2, y2);
	    this.getElements().add(new MoveTo(startX, startY));
	    this.getElements().add(lineTo);
	    oPlane.transitionLength = Math.sqrt((startX-x2)*(startX-x2) + (startY-y2)*(startY-y2));
	}
	
	/*
	 * Calculates landing path
	 */
	public FlightPath(double startX, double startY, double endX, double endY, PlaneElement oPlane) {
		LineTo lineTo = new LineTo(endX, endY);
		this.getElements().add(new MoveTo(startX, startY));
		this.getElements().add(lineTo);
		oPlane.transitionLength = Math.sqrt((startX-endX)*(startX-endX) + (startY-endY)*(startY-endY));
	}
}