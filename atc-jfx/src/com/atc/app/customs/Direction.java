package com.atc.app.customs;

import java.io.Serializable;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;

public class Direction implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2386058712790450105L;
	
	private String name;
	private Integer xCoord;
	private Integer yCoord;
	
	public Direction(String name, Integer xCoord, Integer yCoord) {
		this.name = name;
		this.xCoord = xCoord + 5;
		this.yCoord = yCoord;
	}
	
	public Label createLabel() {
		Label direction = new Label(this.name);
		direction.setLayoutX(this.xCoord);
		direction.setLayoutY(this.yCoord);
		direction.setTextFill(Color.WHITE);
		direction.setTextAlignment(TextAlignment.CENTER);
		return direction;
	}
}
