package com.atc.app.customs;

import java.util.LinkedHashSet;
import java.util.Timer;
import java.util.TimerTask;

import com.atc.app.App;
import com.atc.app.controller.FlightController;
import com.atc.app.controller.MainController;
import com.atc.app.entity.Flight;
import com.atc.app.entity.Report;
import com.atc.app.entity.Runway;

import javafx.animation.Interpolator;
import javafx.animation.PathTransition;
import javafx.application.Platform;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.util.Duration;

public class PlaneElement extends Pane {
	
	private MainController mainController;
	
	private Label flightName;
	private Rectangle flightPointer;
	private ImageView flightIndex;
	public Path currentFlightPath;
	public PathTransition pathTransition;
	private Flight oFlight;
	private Runway oRunway;
	private final static double moveSpeed = 180;
	//private final static double moveSpeed = 50;
	private Color flightColor = Color.LIGHTBLUE;
	public double transitionLength;
	public String flightWaypoint;
	
	/*
	 * timer and refresh rate
	 */
	private Timer timer;
	private static Integer refreshRate = 1000;
	
	/*
	 * fuel timer refresh rate
	 */
	private Timer fuelTimer;
	private final Integer fuelRefreshRate = 60000;
	
	/*
	 * new altitude temp variable and const climb rate
	 */
	private Integer newAltitude = 0;
	private Integer climbRate;
	
	/*
	 * holds runway approaches
	 */
	public LinkedHashSet<String> runwayApproach;
	
	public Integer flightStatus;
	
	class updateDataTask extends TimerTask {
		@Override
		public void run() {
		    Platform.runLater(new Runnable() {
		    	public void run() {
		    		updateData();
		    	}
		    });
		}
	}

	class updateFuelTask extends TimerTask {
		@Override
		public void run() {
		    Platform.runLater(new Runnable() {
		    	public void run() {
		    		updateFuel();
		    	}
		    });
		}
	}
	
	public PlaneElement(Flight oFlight) {
        super();
        this.setPrefSize(1, 1);

        this.mainController = MainController.getMainController();
        this.setId(oFlight.getFlightName());
        this.oFlight = oFlight;
        this.flightName = new Label(oFlight.getFlightName());
        this.flightName.setFont(Font.font ("Verdana", 10));
        this.flightName.setTextFill(flightColor);
        this.flightName.setLayoutY(-15);
        this.flightName.setLayoutX(-10);
        this.setStyle("-fx-cursor: hand;");
        this.climbRate = oFlight.getPlaneType().getPlaneClimbRate();
        this.flightStatus = 1;
        this.flightWaypoint = "none";
        this.runwayApproach = new LinkedHashSet<String>();
        
        this.flightIndex = new ImageView();
        this.flightIndex.setLayoutX(-20);
        this.flightIndex.setLayoutY(-13);
        
        this.flightPointer = new Rectangle(-2, -2, 4, 4);
        this.flightPointer.setFill(flightColor);
        
        this.getChildren().addAll(flightName, flightPointer, flightIndex);     

        this.setOnMouseClicked(new EventHandler<MouseEvent>(){
        	
            @Override
            public void handle(MouseEvent event) {
            	mainController.displayingDetails = true;
            	mainController.getFlightsTableView().requestFocus();
            	mainController.getFlightsTableView().getSelectionModel().select(oFlight);
            }
        });
        this.currentFlightPath = new FlightPath(oFlight.getFlightLongitude(), oFlight.getFlightLatitude(), oFlight.getFlightBearing(), this);
        this.pathTransition = new PathTransition();
        this.pathTransition.setDuration(Duration.millis(this.transitionLength * moveSpeed));
        this.pathTransition.setPath(this.currentFlightPath);
        this.pathTransition.setInterpolator(Interpolator.LINEAR);
        this.pathTransition.setNode(this);
        this.pathTransition.setOnFinished(new EventHandler<ActionEvent>(){
 
            @Override
            public void handle(ActionEvent arg0) {
            	switch (flightStatus) {
				case 0:
					if(oFlight.getFlightBearing() < 180) {
        				generateFlightPath(getTranslateX(), getTranslateY(), oRunway.getRunwayEndX() + FlightController.getMaxXCoord()/2, oRunway.getRunwayEndY() + FlightController.getMaxYCoord()/2, 0, false);
        			}
        			else {
        				generateFlightPath(getTranslateX(), getTranslateY(), oRunway.getRunwayStartX() + FlightController.getMaxXCoord()/2, oRunway.getRunwayStartY() + FlightController.getMaxYCoord()/2, 0, false);
        			}
            		flightStatus = 2;
					break;
				case 1:
					generateFlightPath(getTranslateX(), getTranslateY(), 0, 0, oFlight.getFlightBearing(), true);
					break;
				case 2:
					if(oFlight.getDestination().getAirportId() == App.airport.getAirportId()) {
            			saveReport(1, null);
            		}
            		else {
            			saveReport(3, null);
            		}
					removeFlight();
					break;
				}
            }
        });
        
        this.timer = new Timer();
    	this.timer.schedule(new updateDataTask(), 0, refreshRate);
    	
    	this.fuelTimer = new Timer();
    	this.fuelTimer.schedule(new updateFuelTask(), 0, fuelRefreshRate);
    }
    
	public double getMoveSpeed() {
		return moveSpeed;
	}
	
	private void generateFlightPath(double x1, double y1, double x2, double y2, double angle, boolean type) {
		if(type) {
			this.currentFlightPath = new FlightPath(x1, y1, angle, this);
		}
		else {
			this.currentFlightPath = new FlightPath(x1, y1, x2, y2, this);
		}
		this.pathTransition.setDuration(Duration.millis(this.transitionLength * moveSpeed));
		this.pathTransition.setPath(this.currentFlightPath);
		this.pathTransition.play();
	}
	
	public void changeFlightBearing(Integer angle, Integer newBearing, Flight selectedFlight, String turn) {
		double startX = this.translateXProperty().get();
		double startY = this.translateYProperty().get();
		this.pathTransition.stop();
		this.currentFlightPath = new TurnPath(selectedFlight.getFlightBearing(), newBearing, angle, turn, startX, startY);
		double arcPerimeterPart = (2 * Math.PI * TurnPath.getArcRadius()) / 360;
		this.flightStatus = 1;
		this.transitionLength = angle * arcPerimeterPart;
		this.pathTransition.setDuration(Duration.millis(this.transitionLength * moveSpeed));
		this.pathTransition.setPath(this.currentFlightPath);
		this.pathTransition.play();
	}
	
	public void setFlightAltitude(Integer newAltitude) {
		this.newAltitude = newAltitude;
		if(newAltitude > this.oFlight.getFlightAltitude()) {
			climbRate = Math.abs(climbRate);
		}
		else {
			climbRate = -1 * Math.abs(climbRate);
		}
	}
	
	private void updateData() {
		if(isOutOfRange() && this.translateXProperty().get() != 0) {
			if(this.flightWaypoint.equals(this.oFlight.getFlightWaypoint())) {
				saveReport(2, null);
			}
			else {
				saveReport(4, null);
			}
			removeFlight();
		}
		else {
			this.oFlight.setFlightLatitude((int)this.translateYProperty().get());
			this.oFlight.setFlightLongitude((int)this.translateXProperty().get());
			int listIndex = mainController.getFlightsTableView().getSelectionModel().getSelectedIndex();
			int index = FlightController.getFlightList().indexOf(this.oFlight);
			FlightController.getFlightList().set(index, this.oFlight);
			mainController.getFlightsTableView().getSelectionModel().select(listIndex);

			changeAltitude();
			if(this.flightStatus == 0) {
				setFlightAltitude(App.airport.getAirportElevation());
			}
		}
	}
	
	private void changeAltitude() {
		if(this.newAltitude != 0) {
			int altDiff = this.newAltitude - this.oFlight.getFlightAltitude();
			if(Math.abs(altDiff) < Math.abs(climbRate)) {
				this.oFlight.setFlightAltitude(this.oFlight.getFlightAltitude() + altDiff);
				this.newAltitude = 0;
				changeFlightIndex(this.newAltitude);
			}
			else {
				this.oFlight.setFlightAltitude(this.oFlight.getFlightAltitude() + climbRate);
				changeFlightIndex(altDiff);
			}
		}
	}
	
	private void changeFlightIndex(int altDiff) {
		if(altDiff < 0) {
			this.flightIndex.setImage(new Image("/images/arrow_down.png"));
		}
		else if(altDiff > 0) {
			this.flightIndex.setImage(new Image("/images/arrow_up.png"));
		}
		else {
			this.flightIndex.setImage(null);
		}
	}
	
	private boolean isOutOfRange() {
		if(this.translateXProperty().get() < App.waypointHeight || this.translateXProperty().get() > (FlightController.getMaxXCoord() - App.waypointHeight) || this.translateYProperty().get() < App.waypointHeight || this.translateYProperty().get() > (FlightController.getMaxYCoord() - App.waypointHeight)) {
			return true;
		}
		return false;
	}
	
	public void removeFlight() {
		this.timer.cancel();
		this.fuelTimer.cancel();
		FlightController.getFlightList().remove(this.oFlight);
		if(FlightController.getFlightList().size() == 0 || this.oFlight.getFlightName().equals(mainController.getSelectedFlight().getFlightName())) {
			mainController.setSelectedFlight(null);
		}
		this.pathTransition.stop();
		mainController.getFlightsPane().getChildren().remove(this);
		FlightController.aiCount--;
	}
	
	public String landOnRunway(Runway oRunway) {
		if(this.runwayApproach.contains(oRunway.getRunwayName() + String.valueOf(this.oFlight.getFlightBearing())) && this.oFlight.getFlightAltitude() <= (App.airport.getAirportElevation() + FlightController.initialAlt)) {
			this.pathTransition.stop();
			if(this.oFlight.getFlightBearing() < 180) {
				generateFlightPath(this.getTranslateX(), this.getTranslateY(), oRunway.getRunwayStartX() + FlightController.getMaxXCoord()/2, oRunway.getRunwayStartY() + FlightController.getMaxYCoord()/2, 0, false);
			}
			else {
				generateFlightPath(this.getTranslateX(), this.getTranslateY(), oRunway.getRunwayEndX() + FlightController.getMaxXCoord()/2, oRunway.getRunwayEndY() + FlightController.getMaxYCoord()/2, 0, false);
			}
			this.oRunway = oRunway;
			this.flightStatus = 0;
			this.pathTransition.play();
			return ("Flight " + this.oFlight.getFlightName() + " is landing on runway " + oRunway.getRunwayName());
		}
		else {
			return "Invalid approach angle or altitude";
		}
	}
	
	private void saveReport(Integer reportType, String crashedWith) {
		Report oReport = new Report(reportType, this.oFlight, App.dispatcher, crashedWith);
		mainController.saveReport(oReport, this.oFlight);
	}
	
	private void updateFuel() {
		if(this.oFlight.getFlightFuel() > 0) {
			this.oFlight.itterateFlightFuel(-1);
		}
		else {
			saveReport(6, null);
			removeFlight();
		}
	}
	
	public BooleanBinding assignBinding(PlaneElement flight1, PlaneElement flight2) {
		
		BooleanBinding intersects = new BooleanBinding() {
        	
            {
                this.bind(flight1.boundsInParentProperty(), flight2.boundsInParentProperty());
            }

            @Override
            protected boolean computeValue() {
                return flight1.getBoundsInParent().intersects(flight2.getBoundsInParent());
            }

        };
        
        intersects.addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if (checkIfCrashed(newValue, flight1.oFlight, flight2.oFlight)) {
                	flight1.saveReport(5, flight2.oFlight.getFlightName());
                	flight1.removeFlight();
                	flight2.removeFlight();
                }
            }

        });
        
		return intersects;
	}
	
	private boolean checkIfCrashed(boolean intersect, Flight flight1, Flight flight2) {
		if(intersect && checkIfOverlap(flight1, flight2)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	private static boolean checkIfOverlap(Flight flight1, Flight flight2) {
		int x1 = flight1.getFlightAltitude() - flight1.getPlaneType().getPlaneClearance();
		int x2 = flight1.getFlightAltitude() + flight1.getPlaneType().getPlaneClearance();
		int y1 = flight2.getFlightAltitude() - flight2.getPlaneType().getPlaneClearance();
		int y2 = flight2.getFlightAltitude() + flight2.getPlaneType().getPlaneClearance();	
		return x1 <= y2 && y1 <= x2;
	}
}