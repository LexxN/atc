package com.atc.app.customs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import com.atc.app.App;

import javafx.beans.binding.BooleanBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class Waypoint extends Polygon {
	private Integer[] coords;
	private Double angle;
	private Integer index;
	private Double[] leftBorderCoords;
	private Double[] rightBorderCoords;
	
	static class Point {
		public double x;
		public double y;
		
		public Point(double x, double y) {
			this.x = x;
			this.y = y;
		}
	}
	
	public Waypoint(Integer[] coords, Double angle, Integer index) {
		this.coords = coords;
		this.angle = Math.toRadians(angle);
		this.index = index;
		this.leftBorderCoords = concat(rotatePoint(new Point(100.0, 0.0), new Point(130.0, App.waypointHeight/2)), rotatePoint(new Point(80.0, App.waypointHeight), new Point(130.0, App.waypointHeight/2)));
		this.rightBorderCoords = concat(rotatePoint(new Point(160.0, 0.0), new Point(130.0, App.waypointHeight/2)), rotatePoint(new Point(180.0, App.waypointHeight), new Point(130.0, App.waypointHeight/2)));
		
		this.getPoints().addAll(rotatePoint(new Point(100.0, 0.0), new Point(130.0, App.waypointHeight/2)));
		this.getPoints().addAll(rotatePoint(new Point(160.0, 0.0), new Point(130.0, App.waypointHeight/2)));
		this.getPoints().addAll(rotatePoint(new Point(180.0, App.waypointHeight), new Point(130.0, App.waypointHeight/2)));
		this.getPoints().addAll(rotatePoint(new Point(80.0, App.waypointHeight), new Point(130.0, App.waypointHeight/2)));

		this.setLayoutX(coords[0]);
		this.setLayoutY(coords[1]);
		this.setStroke(Color.web("#0A1728"));
		this.setStrokeWidth(2);
		this.setFill(Color.TRANSPARENT);
	}

	public Double[] rotatePoint(Point pt, Point center) {
		Double[] points = new Double[2];
		double x1 = pt.x - center.x;
		double y1 = pt.y - center.y;

		points[0] = x1 * Math.cos(this.angle) - y1 * Math.sin(this.angle);
		points[1] = x1 * Math.sin(this.angle) + y1 * Math.cos(this.angle);
	    return points;
	}
	
	private static Double[] concat(Double[] first, Double[] second) {
		  Double[] result = Arrays.copyOf(first, first.length + second.length);
		  System.arraycopy(second, 0, result, first.length, second.length);
		  Arrays.toString(result);
		  return result;
	}
	
	public Collection<Line> createBorders() {
		Collection<Line> borders = new ArrayList<Line>();
		borders.add(drawBorder(this.leftBorderCoords));
		borders.add(drawBorder(this.rightBorderCoords));
		return borders;
	}
	
	private Line drawBorder(Double[] data) {
		Line border = new Line(data[0], data[1], data[2], data[3]);
		border.setLayoutX(this.coords[0]);
		border.setLayoutY(this.coords[1]);
		border.setStroke(Color.WHITE);
		border.setStrokeWidth(2);
		return border;
	}
	
	public Label createName() {
		Label waypoint = new Label("W" + this.index);
		waypoint.setLayoutX(this.coords[0] - 8);
		waypoint.setLayoutY(this.coords[1] - 5);
		waypoint.setFont(Font.font("Arial", 11));
		waypoint.setTextFill(Color.WHITE);
		waypoint.setTextAlignment(TextAlignment.CENTER);
		return waypoint;
	}
	
	public BooleanBinding assignBinding(Waypoint waypoint, PlaneElement flight) {
		
		BooleanBinding intersects = new BooleanBinding() {
        	
            {
                this.bind(waypoint.boundsInParentProperty(), flight.boundsInParentProperty());
            }

            @Override
            protected boolean computeValue() {
                return waypoint.getBoundsInParent().intersects(flight.getBoundsInParent());
            }

        };
        
        intersects.addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if (newValue) {
                	flight.flightWaypoint = "W" + index;
                }
                else {
                	flight.flightWaypoint = null;
                }
            }

        });
        
		return intersects;
	}
}
