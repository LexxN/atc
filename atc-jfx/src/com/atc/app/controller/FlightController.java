package com.atc.app.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import com.atc.app.App;
import com.atc.app.customs.Direction;
import com.atc.app.customs.LandingArea;
import com.atc.app.customs.PlaneElement;
import com.atc.app.customs.Waypoint;
import com.atc.app.entity.Airport;
import com.atc.app.entity.Flight;
import com.atc.app.entity.Plane;
import com.atc.app.entity.Runway;
import com.atc.app.service.AirportService;
import com.atc.app.service.AirportServiceImpl;
import com.atc.app.service.PlaneService;
import com.atc.app.service.PlaneServiceImpl;
import com.atc.app.service.RunwayService;
import com.atc.app.service.RunwayServiceImpl;

public class FlightController {
	
	private MainController mainController;
	
	private PlaneService planeService;
	private AirportService airportService;
	private RunwayService runwayService;
	
	protected static ObservableList<Flight> flightList = FXCollections.observableArrayList();
	private static ObservableList<PlaneElement> planesList = FXCollections.observableArrayList();
	private List<Plane> planesTypeList;
	private List<Airport> airportsList;
	protected static List<Runway> airportRunwayList;
	
	/*
	 * character pool and randomGenerator initialization
	 */
	private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private Random randomGenerator = new Random();
	
	/*
	 * parameters holding max&min coordinates(x,y), altitude(ft), fuel(mins) values, initial altitude(ft)
	 */
	private static int maxXCoord;
	private static int maxYCoord;
	public static final int maxAlt = 30000;
	private int minAlt;
	private final int maxFuel = 30;
	private final int minFuel = 10;
	private final int maxTakeOffFuel = 180;
	public final static int initialAlt = 1000;
	private final int arrivalsMaxAlt = 10000;
	public final static int landingMaxAlt = 300;

	/*
	 * world directions & waypoints
	 */
	private List<Direction> directionsList;
	private static List<Waypoint> waypointsList;
	
	/*
	 * ai count, aitimer and refresh rate
	 */
	public static int aiCount = 0;
	private Timer timer;
	private static final Integer refreshRate = 25000;
	
	class updateAICountTask extends TimerTask {
		@Override
		public void run() {
		    Platform.runLater(new Runnable() {
		    	public void run() {
		    		if(aiCount < App.airport.getAirportAICount()) {
		    			generateFlight(randomGenerator.nextBoolean());
		    			//generateFlight(true);
		    			//generateFlight(false);
		    			aiCount++;
		    		}
		    	}
		    });
		}
	}
	
	public FlightController() {
		this.mainController = MainController.getMainController();
		this.planeService = new PlaneServiceImpl();
		this.airportService = new AirportServiceImpl();
		this.runwayService = new RunwayServiceImpl();
		this.planesTypeList = planeService.listPlaneTypes();
		airportRunwayList = runwayService.listRunways();
		this.airportsList = airportService.listAirports();
		maxXCoord = (int) (mainController.mainGrid.prefWidthProperty().get() * 8/10);
		maxYCoord = (int) mainController.mainGrid.prefHeightProperty().get();
		this.minAlt = App.airport.getAirportElevation() + 300;
		this.directionsList = new ArrayList<Direction>();
		this.directionsList.add(new Direction("W", maxXCoord - App.waypointHeight, maxYCoord/2));
		this.directionsList.add(new Direction("E", 0, maxYCoord/2));
		this.directionsList.add(new Direction("N", maxXCoord/2, 0));
		this.directionsList.add(new Direction("S", maxXCoord/2, maxYCoord - App.waypointHeight));
		
		waypointsList = new ArrayList<Waypoint>();
		waypointsList.add(new Waypoint(new Integer[]{maxXCoord/4, App.waypointHeight/2}, 0.0, 8));
		waypointsList.add(new Waypoint(new Integer[]{(int) (maxXCoord*0.75), App.waypointHeight/2}, 0.0, 1));
		waypointsList.add(new Waypoint(new Integer[]{maxXCoord - App.waypointHeight/2, maxYCoord/4}, 90.0, 2));
		waypointsList.add(new Waypoint(new Integer[]{maxXCoord - App.waypointHeight/2, (int) (maxYCoord*0.75)}, 90.0, 3));
		waypointsList.add(new Waypoint(new Integer[]{(int) (maxXCoord*0.75), maxYCoord - App.waypointHeight/2}, 180.0, 4));
		waypointsList.add(new Waypoint(new Integer[]{maxXCoord/4, maxYCoord - App.waypointHeight/2}, 180.0, 5));
		waypointsList.add(new Waypoint(new Integer[]{App.waypointHeight/2, (int) (maxYCoord*0.75)}, 270.0, 6));
		waypointsList.add(new Waypoint(new Integer[]{App.waypointHeight/2, maxYCoord/4}, 270.0, 7));
		
		mainController.displayAirport(airportRunwayList, maxXCoord, maxYCoord);
		mainController.displayBorder(maxXCoord, maxYCoord);
		mainController.displayWaypoints(directionsList, waypointsList, maxXCoord, maxYCoord);
		
		this.timer = new Timer();
    	this.timer.schedule(new updateAICountTask(), 0, refreshRate);
	}
	
	public static int getMaxXCoord() {
		return maxXCoord;
	}

	public static int getMaxYCoord() {
		return maxYCoord;
	}
	
	public static final ObservableList<Flight> getFlightList() {
		return flightList;
	}

	public static final List<Waypoint> getWaypointsList() {
		return waypointsList;
	}

	public static final List<Runway> getAirportRunwayList() {
		return airportRunwayList;
	}
	
	// generates random flight, type = false outgoing, type = true incoming
	private void generateFlight(boolean type) {
		int index, sector;
		long bearing;
		Plane oPlane = new Plane();
		Airport oAirport = new Airport();
		Flight oFlight = new Flight();

		index = randomGenerator.nextInt(planesTypeList.size());
		oPlane = planesTypeList.get(index);
		index = randomGenerator.nextInt(airportsList.size());
		oAirport = airportsList.get(index);
		oFlight.setFlightName(randomName(4));
		oFlight.setPlaneType(oPlane);
		oFlight.setFlightDispatcher(App.dispatcher);
		
		bearing = Math.round(calculateBearing(App.airport.getAirportLatitude(), App.airport.getAirportLongitude(), oAirport.getAirportLatitude(), oAirport.getAirportLongitude()));
		sector = calculateSector(bearing);
		if(type) {
			int result[] = generateStartCoords(maxXCoord, maxYCoord, sector);
			oFlight.setFlightLatitude(randomGenerator.nextInt((result[3] - result[2]) + 1) + result[2]);
			oFlight.setFlightLongitude(randomGenerator.nextInt((result[1] - result[0]) + 1) + result[0]);
			oFlight.setFlightBearing(result[4]);
			/*oFlight.setFlightLatitude(maxYCoord - 150);
			oFlight.setFlightLongitude(maxXCoord / 2 + 100);
			oFlight.setFlightBearing((int) 270);*/
			oFlight.setFlightAltitude(randomGenerator.nextInt((arrivalsMaxAlt - this.minAlt) + 1) + this.minAlt);
			oFlight.setFlightFuel(randomGenerator.nextInt((this.maxFuel - this.minFuel) + 1) + this.minFuel);
			oFlight.setDestination(App.airport);
			oFlight.setOrigin(oAirport);
			oFlight.setFlightWaypoint("airfield");
			displayFlight(oFlight, false);
			mainController.warningMessageLabel.setText("Flight " + oFlight.getFlightName() + " is requesting landing assistance.");
		}
		else {
			oFlight.setFlightLatitude(0);
			oFlight.setFlightLongitude(0);
			oFlight.setFlightBearing(null);
			oFlight.setFlightAltitude(App.airport.getAirportElevation());
			oFlight.setFlightFuel(maxTakeOffFuel);
			oFlight.setDestination(oAirport);
			oFlight.setOrigin(App.airport);
			oFlight.setFlightWaypoint("W" + String.valueOf(sector));
			mainController.warningMessageLabel.setText("Flight " + oFlight.getFlightName() + " is requesting runway clearance.");
		}
		flightList.add(oFlight);
		
		mainController.getFlightsTableView().setItems(flightList);
	}
	
	private String randomName(int length) {
		StringBuilder sb = new StringBuilder(length);
		for(int i = 0; i < length; i++)
			sb.append(AB.charAt(randomGenerator.nextInt(AB.length())));
		return sb.toString();
	}
	
	// Calculates the initial bearing
	private double calculateBearing(double lat1, double long1, double lat2, double long2){
		double latitude1 = Math.toRadians(lat1);
		double latitude2 = Math.toRadians(lat2);
		double longDiff= Math.toRadians(long2-long1);
		double y= Math.sin(longDiff)*Math.cos(latitude2);
		double x=Math.cos(latitude1)*Math.sin(latitude2)-Math.sin(latitude1)*Math.cos(latitude2)*Math.cos(longDiff);

		return (Math.toDegrees(Math.atan2(y, x))+360)%360;
	}
	
	private int calculateSector(long bearing) {
		int sector = 0;
		for(int i = 0; i < 8; i++) {
    		if(bearing > i*45 && bearing <= (i*45 + 45)){
    			sector = i;
    			break;
    		}
    	}
		return sector;
	}

	private int[] generateStartCoords(int maxWidth, int maxHeight, int sector) {
    	int array[] = null;
    	
    	switch (sector) {
		case 0:
			array = new int[]{maxWidth/2, maxWidth - App.waypointHeight, App.waypointHeight, App.waypointHeight, 180};
			break;
		case 1:
			array = new int[]{maxWidth - App.waypointHeight, maxWidth - App.waypointHeight, App.waypointHeight, maxHeight/2, 270};
			break;
		case 2:
			array = new int[]{maxWidth - App.waypointHeight, maxWidth - App.waypointHeight, maxHeight/2, maxHeight - App.waypointHeight, 270};
			break;
		case 3:
			array = new int[]{maxWidth/2, maxWidth - App.waypointHeight, maxHeight - App.waypointHeight, maxHeight - App.waypointHeight, 360};
			break;
		case 4:
			array = new int[]{App.waypointHeight, maxWidth/2, maxHeight - App.waypointHeight, maxHeight - App.waypointHeight, 360};
			break;
		case 5:
			array = new int[]{App.waypointHeight, App.waypointHeight, maxHeight - App.waypointHeight, maxHeight/2, 90};
			break;
		case 6:
			array = new int[]{App.waypointHeight, App.waypointHeight, maxHeight/2, App.waypointHeight, 90};
			break;
		case 7:
			array = new int[]{App.waypointHeight, maxWidth/2, App.waypointHeight, App.waypointHeight, 180};
			break;
		}
    	return array;
    }

	protected static void displayFlight(Flight oFlight, boolean takingOff) {
		PlaneElement flight = new PlaneElement(oFlight);
		flight.setId(String.valueOf(oFlight.getFlightName()));
		MainController.getMainController().flightsPane.getChildren().addAll(flight);
		checkIfTakingOff(flight, takingOff);
		assignBindings(flight);
		planesList.add(flight);
		flight.pathTransition.play();
	}
	
	private static void checkIfTakingOff(PlaneElement flight, boolean takingOff) {
		if(takingOff) {
			flight.setFlightAltitude(App.airport.getAirportElevation() + initialAlt);
		}
	}
	
	private static void assignBindings(PlaneElement flight) {
		for (Runway oRunway : airportRunwayList) {
			LandingArea landingArea1 = (LandingArea) MainController.getMainController().flightsPane.lookup("#" + oRunway.getRunwayName() + String.valueOf(oRunway.getRunwayLanding1()));
			App.bindings.add(landingArea1.assignBinding(landingArea1, flight));
			LandingArea landingArea2 = (LandingArea) MainController.getMainController().flightsPane.lookup("#" + oRunway.getRunwayName() + String.valueOf(oRunway.getRunwayLanding2()));
			App.bindings.add(landingArea1.assignBinding(landingArea2, flight));				
		}
		
		for (Waypoint oWaypoint : waypointsList) {
			App.bindings.add(oWaypoint.assignBinding(oWaypoint, flight));
		}
		
		for (PlaneElement oPlane : planesList) {
			App.bindings.add(oPlane.assignBinding(oPlane, flight));
		}
	}
}
