package com.atc.app.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import com.atc.app.entity.Report;

public class ReportController implements Initializable {
	
	private ArrayList<String> reportStatuses = new ArrayList<String>();
	private Integer posReportsCount = 0;
	private Integer negReportsCount = 0;
	private Integer crashReportsCount = 0;
	
	@FXML private TableView<Report> reportsTableView;
	@FXML private TableColumn<Report, String> reportFlightCol;
	@FXML private TableColumn<Report, String> reportOriginCol;
	@FXML private TableColumn<Report, String> reportDestinationCol;
	@FXML private TableColumn<Report, String> reportAltitudeCol;
	@FXML private TableColumn<Report, String> reportTypeCol;
	@FXML private Label pReportsLabel;
	@FXML private Label nReportsLabel;
	@FXML private Label cReportsLabel;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		reportStatuses.add("Landed successfully");
		reportStatuses.add("Left our airspace successfully");
		reportStatuses.add("Left our airspace without permission");
		reportStatuses.add("Did not reach destination");
		reportStatuses.add("Crashed with flight #");
		reportStatuses.add("Crashed without fuel");
		
		reportFlightCol.setCellValueFactory(
        		cellData -> new SimpleStringProperty(cellData.getValue().getFlight() != null ? cellData.getValue().getFlight().getFlightName() : "null"));
		reportOriginCol.setCellValueFactory(
        		cellData -> new SimpleStringProperty(cellData.getValue().getFlight().getOrigin() != null ? cellData.getValue().getFlight().getOrigin().getAirportName().split("/")[0] : "null"));
		reportDestinationCol.setCellValueFactory(
        		cellData -> new SimpleStringProperty(cellData.getValue().getFlight().getDestination() != null ? cellData.getValue().getFlight().getDestination().getAirportName().split("/")[0] : "null"));
		reportAltitudeCol.setCellValueFactory(
        		cellData -> new SimpleStringProperty(cellData.getValue().getFlight() != null ? cellData.getValue().getFlight().getFlightAltitude() + "ft" : "null"));
		reportTypeCol.setCellValueFactory(
				//cellData -> new SimpleStringProperty(cellData.getValue() != null ? reportStatuses.get(cellData.getValue().getReportType() - 1) : "null")
				cellData -> displayMessage(cellData.getValue())
				);
	}

	public void displayReports(ObservableList<Report> list) {
		reportsTableView.setItems(list);
		for (Report oRecord : list) {
			switch (oRecord.getReportType()) {
			case 1:
			case 2:	
				posReportsCount++;
				break;
			case 3:
			case 4:	
				negReportsCount++;
				break;
			case 5:
			case 6:	
				crashReportsCount++;
				break;
			default:
				break;
			}
		}
		pReportsLabel.setText(String.valueOf(posReportsCount));
		nReportsLabel.setText(String.valueOf(negReportsCount));
		cReportsLabel.setText(String.valueOf(crashReportsCount));
	}
	
	private StringProperty displayMessage(Report oReport) {
		String status = "";
		if(oReport != null) {
			status = reportStatuses.get(oReport.getReportType() - 1);
			status += oReport.getReportType() == 5 ? oReport.getCrashedWith() : "";
		}
		return new SimpleStringProperty(status);
	}
}
