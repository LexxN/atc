package com.atc.app.controller;

import java.net.URL;
import java.util.ResourceBundle;

import com.atc.app.App;
import com.atc.app.entity.User;
import com.atc.app.screenframework.ControlledScreen;
import com.atc.app.screenframework.ScreensController;
import com.atc.app.service.UserService;
import com.atc.app.service.UserServiceImpl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController implements Initializable, ControlledScreen {
	
	ScreensController screenController;
	private UserService userService = new UserServiceImpl();
	private static LoginController loginController;
	
	@FXML private TextField userNameField;
    @FXML private PasswordField userPasswordField;
    
    @Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setScreenParent(ScreensController screenParent) {
		screenController = screenParent;
	}
	
	@Override
	public void setScreenController(Object object) {
		LoginController.loginController = (LoginController) object;
	}

	public static LoginController getLoginController() {
		return loginController;
	}
	
	@FXML
	public void loginButtonAction(ActionEvent event) {
		if((!userNameField.getText().isEmpty()) && (!userPasswordField.getText().isEmpty())) {
			User dispatcher = userService.validateUser(userNameField.getText(), userPasswordField.getText());
			if(dispatcher != null) {
				App.dispatcher = dispatcher;
				App.airport = dispatcher.getAirport();
				screenController.setScreen(App.screen2ID);
				new FlightController();
			}
		}
	}
}
