package com.atc.app.entity;

import java.io.Serializable;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "FLIGHTS")
public class Flight implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7542719508306094869L;
	
	@Id
    @GeneratedValue(generator = "FLIGHT_SEQUENCE", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "FLIGHT_SEQUENCE", sequenceName = "FLIGHT_SEQUENCE", allocationSize = 1)
    @Column(name="FLIGHT_ID", unique = true, nullable = false)
    private Integer flightId;
	@Column(name="FLIGHT_NAME", nullable = false)
	private String flightName;
	@Column(name="FLIGHT_LATITUDE")
	private Integer flightLatitude;
	@Column(name="FLIGHT_LONGITUDE")
	private Integer flightLongitude;
	@Column(name="FLIGHT_ALTITUDE", nullable = false)
	private Integer flightAltitude;
	@Column(name="FLIGHT_BEARING")
	private Integer flightBearing;
	@ManyToOne
    @JoinColumn(name="FLIGHT_ORIGIN", nullable = false)
    private Airport origin;
	@ManyToOne
    @JoinColumn(name="FLIGHT_DESTINATION", nullable = false)
    private Airport destination;
	@Column(name="FLIGHT_REMAINING_FUEL", nullable = false)
    private Integer flightFuel;
	@ManyToOne
    @JoinColumn(name="FLIGHT_PLANE_TYPE", nullable = false)
    private Plane plane;
	@ManyToOne
    @JoinColumn(name="FLIGHT_DISPATCHER", nullable = false)
    private User flight_dispatcher;
	@Column(name="FLIGHT_WAYPOINT")
	private String flightWaypoint;
	
	public Flight() {
		super();
	}
	
	public Flight(String flightName, Integer flightLatitude,
			Integer flightLongitude, Integer flightAltitude, Airport origin, Airport destination,
			Integer flightFuel, Plane plane, String flightWaypoint) {
		super();
		this.flightName = flightName;
		this.flightLatitude = flightLatitude;
		this.flightLongitude = flightLongitude;
		this.flightAltitude = flightAltitude;
		this.origin = origin;
		this.destination = destination;
		this.flightFuel = flightFuel;
		this.plane = plane;
		this.flightWaypoint = flightWaypoint;
	}
	
	public Integer getFlightId() {
		return flightId;
	}
	
	public String getFlightName() {
		return flightName;
	}

	public void setFlightName(String flightName) {
		this.flightName = flightName;
	}
	
	public Integer getFlightLatitude() {
		return flightLatitude;
	}

	public void setFlightLatitude(Integer flightLatitude) {
		this.flightLatitude = flightLatitude;
	}
	
	public Integer getFlightLongitude() {
		return flightLongitude;
	}

	public void setFlightLongitude(Integer flightLongitude) {
		this.flightLongitude = flightLongitude;
	}
	
	public StringProperty getCoordsProperty() {
        return new SimpleStringProperty(String.valueOf(this.getFlightLongitude()) + "x" + String.valueOf(this.getFlightLatitude()));
    }
	
	public Integer getFlightAltitude() {
		return flightAltitude;
	}
	
	public void setFlightAltitude(Integer flightAltitude) {
		this.flightAltitude = flightAltitude;
	}	
	
	public StringProperty getAltitudeProperty() {
		return new SimpleStringProperty(String.valueOf(this.getFlightAltitude()) + "ft");
	}
	
	public Integer getFlightBearing() {
		return flightBearing;
	}

	public void setFlightBearing(Integer flightBearing) {
		this.flightBearing = flightBearing;
	}

	public Airport getOrigin() {
		return origin;
	}
	
	public void setOrigin(Airport origin) {
		this.origin = origin;
	}
	
	public Airport getDestination() {
		return destination;
	}
	
	public void setDestination(Airport destination) {
		this.destination = destination;
	}
	
	public Integer getFlightFuel() {
		return flightFuel;
	}
	
	public void setFlightFuel(Integer flightFuel) {
		this.flightFuel = flightFuel;
	}
	
	public void itterateFlightFuel(Integer index) {
		this.flightFuel += index;
	}
	
	public Plane getPlaneType() {
		return plane;
	}
	
	public void setPlaneType(Plane plane) {
		this.plane = plane;
	}

	public User getFlightDispatcher() {
		return flight_dispatcher;
	}
	
	public void setFlightDispatcher(User dispatcher) {
		this.flight_dispatcher = dispatcher;
	}

	public String getFlightWaypoint() {
		return flightWaypoint;
	}

	public void setFlightWaypoint(String flightWaypoint) {
		this.flightWaypoint = flightWaypoint;
	}
}
