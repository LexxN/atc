package com.atc.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "REPORTS")
public class Report implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 177731528114797511L;
	
	@Id
    @GeneratedValue(generator = "REPORT_SEQUENCE", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "REPORT_SEQUENCE", sequenceName = "REPORT_SEQUENCE", allocationSize = 1)
    @Column(name="REPORT_ID", unique = true, nullable = false)
    private Integer reportId;
    @Column(name="REPORT_TYPE", nullable = false)
    private Integer reportType;
    @OneToOne
    @JoinColumn(name="FLIGHT_ID", nullable = false)
    private Flight flight;
    @ManyToOne
    @JoinColumn(name="USER_ID", nullable = false)
    private User user;
    @Column(name="CRASHED_WITH", nullable = true)
    private String crashedWith;
    
    public Report() {
    	super();
    }

	public Report(Integer reportType, Flight flight, User user, String crashedWith) {
		super();
		this.reportType = reportType;
		this.flight = flight;
		this.user = user;
		this.crashedWith = crashedWith;
	}

	public Integer getReportId() {
		return reportId;
	}

	public Integer getReportType() {
		return reportType;
	}

	public void setReportType(Integer reportType) {
		this.reportType = reportType;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getCrashedWith() {
		return crashedWith;
	}

	public void setCrashedWith(String crashedWith) {
		this.crashedWith = crashedWith;
	}
}