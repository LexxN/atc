package com.atc.app.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PLANES")
public class Plane implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2486630672833340617L;
	@Id
    @GeneratedValue(generator = "PLANE_SEQUENCE", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "PLANE_SEQUENCE", sequenceName = "PLANE_SEQUENCE", allocationSize = 1)
    @Column(name="PLANE_ID", unique = true, nullable = false)
    private Integer planeId;
	@Column(name="PLANE_NAME", unique = true, nullable = false)
    private String planeName;
	@Column(name="PLANE_CLIMBRATE", nullable = false)
	private Integer planeClimbRate;
	@Column(name="PLANE_CLEARANCE", nullable = false)
	private Integer planeClearance;
	@OneToMany(mappedBy="plane")
	private List<Flight> flights;
	
	public Plane() {
        super();          
	}

	public Plane(Integer planeId, String planeName, Integer planeClimbRate, Integer planeClearance) {
		super();
		this.planeId = planeId;
		this.planeName = planeName;
		this.planeClimbRate = planeClimbRate;
		this.planeClearance = planeClearance;
	}

	public Integer getPlaneId() {
		return planeId;
	}

	public void setPlaneId(Integer planeId) {
		this.planeId = planeId;
	}

	public String getPlaneName() {
		return planeName;
	}

	public void setPlaneName(String planeName) {
		this.planeName = planeName;
	}	
	
	public List<Flight> getFlights() {
		return flights;
	}

	public Integer getPlaneClimbRate() {
		return planeClimbRate;
	}

	public void setPlaneClimbRate(Integer planeClimbRate) {
		this.planeClimbRate = planeClimbRate;
	}

	public Integer getPlaneClearance() {
		return planeClearance;
	}

	public void setPlaneClearance(Integer planeClearance) {
		this.planeClearance = planeClearance;
	}
}
