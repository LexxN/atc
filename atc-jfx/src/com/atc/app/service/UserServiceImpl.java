package com.atc.app.service;

import com.atc.app.dao.UserDAO;
import com.atc.app.dao.UserDAOImpl;
import com.atc.app.entity.User;

public class UserServiceImpl implements UserService {

	private UserDAO userDAO = new UserDAOImpl();
	
	@Override
	public User validateUser(String userName, String userPassword) {
		return userDAO.validateUser(userName, userPassword);
	}

}
