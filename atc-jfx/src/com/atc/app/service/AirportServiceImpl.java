package com.atc.app.service;

import java.util.List;

import com.atc.app.dao.AirportDAO;
import com.atc.app.dao.AirportDAOImpl;
import com.atc.app.entity.Airport;

public class AirportServiceImpl implements AirportService {

	private AirportDAO airportDAO = new AirportDAOImpl();
	
	@Override
	public List<Airport> listAirports() {
		return airportDAO.listAirports();
	}

}
