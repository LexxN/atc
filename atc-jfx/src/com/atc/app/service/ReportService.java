package com.atc.app.service;

import java.util.List;

import com.atc.app.entity.Report;

public interface ReportService {
	public void addReport(Report report);
	public List<Report> listReports();
}
