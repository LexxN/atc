package com.atc.app.service;

import java.util.List;
import com.atc.app.entity.Runway;

public interface RunwayService {
	public List<Runway> listRunways();
}
