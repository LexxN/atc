package com.atc.app.service;

import com.atc.app.entity.User;

public interface UserService {
	public User validateUser(String userName, String userPassword);
}
