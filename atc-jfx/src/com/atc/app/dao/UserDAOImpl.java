package com.atc.app.dao;

import com.atc.app.entity.HibernateUtil;
import com.atc.app.entity.User;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class UserDAOImpl implements UserDAO {
	
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	public User validateUser(String userName, String userPassword) {
		String encrPassword = User.cryptWithMD5(userPassword);
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.beginTransaction();
		User user = (User) currentSession.createCriteria(User.class)
				.add(Restrictions.eq("userName", userName))
				.add(Restrictions.eq("userPassword", encrPassword))
				.uniqueResult();
		currentSession.getTransaction().commit();
		return user;
	}

}
