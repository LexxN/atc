package com.atc.app.dao;

import java.util.List;

import com.atc.app.entity.Plane;

public interface PlaneDAO {
	public List<Plane> listPlaneTypes();
}
