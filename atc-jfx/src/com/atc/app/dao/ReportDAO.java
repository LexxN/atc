package com.atc.app.dao;

import java.util.List;

import com.atc.app.entity.Report;

public interface ReportDAO {
	public void addReport(Report report);
	public List<Report> listReports();
}
