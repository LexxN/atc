package com.atc.app.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.atc.app.entity.HibernateUtil;
import com.atc.app.entity.Plane;

public class PlaneDAOImpl implements PlaneDAO {
	
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	@SuppressWarnings("unchecked")
	@Override
	public List<Plane> listPlaneTypes() {
		List <Plane> list = new ArrayList<>();
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.beginTransaction();
		list = currentSession.createQuery("FROM Plane").list();
        currentSession.getTransaction().commit();
        return list; 
	}

}
