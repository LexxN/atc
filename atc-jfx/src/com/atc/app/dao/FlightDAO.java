package com.atc.app.dao;

import com.atc.app.entity.Flight;

public interface FlightDAO {
	public void addFlight(Flight flight);
	public void removeFlight(Integer flightID);
	public void updateFlight(Flight flight);
}
