package com.atc.app.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.atc.app.App;
import com.atc.app.entity.Airport;
import com.atc.app.entity.HibernateUtil;

public class AirportDAOImpl implements AirportDAO {

	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Airport> listAirports() {
		List <Airport> list = new ArrayList<>();
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.beginTransaction();
		list = currentSession.createQuery("FROM Airport WHERE Airport_ID != :airport")
				.setParameter("airport", App.airport.getAirportId())
				.list();
        currentSession.getTransaction().commit();
        return list; 
	}

}
