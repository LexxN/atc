package com.atc.app.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.atc.app.App;
import com.atc.app.entity.HibernateUtil;
import com.atc.app.entity.Runway;

public class RunwayDAOImpl implements RunwayDAO {
	
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	@SuppressWarnings("unchecked")
	@Override
	public List<Runway> listRunways() {
		List<Runway> list = new ArrayList<>();
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.beginTransaction();
		list = currentSession.createQuery("FROM Runway WHERE Airport_ID = :airport")
				.setParameter("airport", App.airport.getAirportId())
				.list();
        currentSession.getTransaction().commit();
        return list; 
	}

}
