package com.atc.app.entity;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "USERS")
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5257584504951790515L;
	@Id
    @GeneratedValue(generator = "USER_SEQUENCE", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "USER_SEQUENCE", sequenceName = "USER_SEQUENCE", allocationSize = 1)
    @Column(name="USER_ID", unique = true, nullable = false)
    private Integer userId;
    @Column(name="USER_NAME", unique = true, nullable = false)
    private String userName;
    @Column(name="USER_PASSWORD", nullable = false)
    private String userPassword;
    @Column(name="USER_LEVEL", nullable = false)
    private Integer userLevel;
    @ManyToOne
    @JoinColumn(name="AIRPORT_ID", nullable = false)
    private Airport airport;
    @OneToMany(mappedBy="flight_dispatcher")
    private List<Flight> flights;
    
    @Transient
    private static MessageDigest md;
    
    public User() {
        super();          
	}

	public User(String userName, String userPassword, Integer userLevel,
			Airport airport) {
		super();
		this.userName = userName;
		this.userPassword = userPassword;
		this.userLevel = userLevel;
		this.airport = airport;
	}

	public Integer getUserId() {
		return userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public Integer getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(Integer userLevel) {
		this.userLevel = userLevel;
	}

	public Airport getAirport() {
		return airport;
	}	
	
	public List<Flight> getFlights() {
		return flights;
	}

	public void setFlights(List<Flight> flights) {
		this.flights = flights;
	}
	
	public static String cryptWithMD5(String pass){
		try {
			md = MessageDigest.getInstance("MD5");
			byte[] passBytes = pass.getBytes();
			md.reset();
		    byte[] digested = md.digest(passBytes);
			StringBuffer sb = new StringBuffer();
			for(int i=0;i<digested.length;i++){
		    	sb.append(Integer.toHexString(0xff & digested[i]));
			}
		        return sb.toString();
		} catch (NoSuchAlgorithmException ex) {
		        Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;

	 }
}
