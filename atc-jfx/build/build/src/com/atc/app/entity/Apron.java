package com.atc.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "APRONS")
public class Apron implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2713522199584907164L;

	@Id
    @GeneratedValue(generator = "APRON_SEQUENCE", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "APRON_SEQUENCE", sequenceName = "APRON_SEQUENCE", allocationSize = 1)
	@Column(name="APRON_ID", unique = true, nullable = false)
    private Integer apronId;
	@ManyToOne
    @JoinColumn(name="TERMINAL_ID", nullable = false)
    private Terminal apronTerminal;
	@Column(name="APRON_X", nullable = false)
    private Integer apronX;
	@Column(name="APRON_Y", nullable = false)
    private Integer apronY;

	public Apron() {
		super();
	}

	public Apron(Terminal apronTerminal, Integer apronX,
			Integer apronY) {
		super();
		this.apronTerminal = apronTerminal;
		this.apronX = apronX;
		this.apronY = apronY;
	}

	public Integer getApronId() {
		return apronId;
	}

	public Terminal getApronTerminal() {
		return apronTerminal;
	}

	public void setApronTerminal(Terminal apronTerminal) {
		this.apronTerminal = apronTerminal;
	}

	public Integer getApronX() {
		return apronX;
	}

	public void setApronX(Integer apronX) {
		this.apronX = apronX;
	}

	public Integer getApronY() {
		return apronY;
	}

	public void setApronY(Integer apronY) {
		this.apronY = apronY;
	}

	
}
