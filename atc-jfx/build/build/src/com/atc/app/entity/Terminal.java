package com.atc.app.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TERMINALS")
public class Terminal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1503053493025626139L;
	
	@Id
    @GeneratedValue(generator = "TERMINAL_SEQUENCE", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "TERMINAL_SEQUENCE", sequenceName = "TERMINAL_SEQUENCE", allocationSize = 1)
	@Column(name="TERMINAL_ID", unique = true, nullable = false)
    private Integer terminalId;
	@ManyToOne
    @JoinColumn(name="AIRPORT_ID", nullable = false)
    private Airport terminalAirport;
	@Column(name="TERMINAL_X", nullable = false)
    private Integer terminalX;
	@Column(name="TERMINAL_Y", nullable = false)
    private Integer terminalY;
	@OneToMany(mappedBy="apronTerminal")
    private List<Apron> aprons;
	
	public Terminal() {
		super();
	}

	public Terminal(Airport terminalAirport,
			Integer terminalX, Integer terminalY) {
		super();
		this.terminalAirport = terminalAirport;
		this.terminalX = terminalX;
		this.terminalY = terminalY;
	}

	public Integer getTerminalId() {
		return terminalId;
	}

	public Airport getTerminalAirport() {
		return terminalAirport;
	}

	public void setTerminalAirport(Airport terminalAirport) {
		this.terminalAirport = terminalAirport;
	}

	public Integer getTerminalX() {
		return terminalX;
	}

	public void setTerminalX(Integer terminalX) {
		this.terminalX = terminalX;
	}

	public Integer getTerminalY() {
		return terminalY;
	}

	public void setTerminalY(Integer terminalY) {
		this.terminalY = terminalY;
	}

	
}
