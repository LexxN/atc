package com.atc.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import java.util.List;

@Entity
@Table(name = "AIRPORTS")
public class Airport implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4724120031216199639L;
	@Id
    @GeneratedValue(generator = "AIRPORT_SEQUENCE", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "AIRPORT_SEQUENCE", sequenceName = "AIRPORT_SEQUENCE", allocationSize = 1)
    @Column(name="AIRPORT_ID", unique = true, nullable = false)
    private Integer airportId;
    @Column(name="AIRPORT_NAME", unique = true, nullable = false)
    private String airportName;
    @Column(name="AIRPORT_LATITUDE", nullable = false)
    private Double airportLatitude;
    @Column(name="AIRPORT_LONGITUDE", nullable = false)
    private Double airportLongitude;
    @Column(name="AIRPORT_ELEVATION", nullable = false)
    private Integer airportElevation;
    @Column(name="AIRPORT_AI_COUNT", nullable = false)
    private Integer airportAICount;
    @OneToMany(mappedBy="airport")
    private List<User> dispatchers;
    @OneToMany(mappedBy="origin")
    private List<Flight> outgoingFlights;
    @OneToMany(mappedBy="destination")
    private List<Flight> incomingFlights;
    @OneToMany(mappedBy="runwayAirport")
    private List<Runway> runways;
    @OneToMany(mappedBy="terminalAirport")
    private List<Terminal> terminals;

	public Airport() {
        super();          
	}
    
	public Airport(String airportName, Double airportLatitude, Double airportLongitude, Integer airportElevation) {
		super();
		this.airportName = airportName;
		this.airportLatitude = airportLatitude;
		this.airportLongitude = airportLongitude;
		this.airportElevation = airportElevation;
	}

	public Integer getAirportId() {
		return airportId;
	}

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	public Double getAirportLatitude() {
		return airportLatitude;
	}

	public void setAirportLatitude(Double airportLatitude) {
		this.airportLatitude = airportLatitude;
	}
	
	public Double getAirportLongitude() {
		return airportLongitude;
	}

	public void setAirportLongitude(Double airportLongitude) {
		this.airportLongitude = airportLongitude;
	}
	
	public Integer getAirportElevation() {
		return airportElevation;
	}

	public void setAirportElevation(Integer airportElevation) {
		this.airportElevation = airportElevation;
	}	

	public Integer getAirportAICount() {
		return airportAICount;
	}

	public void setAirportAICount(Integer airportAICount) {
		this.airportAICount = airportAICount;
	}

	public List<User> getDispatchers() {
		return dispatchers;
	}

	public void setDispatchers(List<User> dispatchers) {
		this.dispatchers = dispatchers;
	}

	public List<Flight> getOutgoingFlights() {
		return outgoingFlights;
	}

	public void setOutgoingFlights(List<Flight> outgoingFlights) {
		this.outgoingFlights = outgoingFlights;
	}

	public List<Flight> getIncomingFlights() {
		return incomingFlights;
	}

	public void setIncomingFlights(List<Flight> incomingFlights) {
		this.incomingFlights = incomingFlights;
	}
	
	public List<Runway> getRunways() {
		return runways;
	}

	public void setRunways(List<Runway> runways) {
		this.runways = runways;
	}

	public List<Terminal> getTerminals() {
		return terminals;
	}

	public void setTerminals(List<Terminal> terminals) {
		this.terminals = terminals;
	}
}
