package com.atc.app.service;

import com.atc.app.entity.Plane;
import java.util.List;

public interface PlaneService {
	public List<Plane> listPlaneTypes();
}
