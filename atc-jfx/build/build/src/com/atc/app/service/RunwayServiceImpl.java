package com.atc.app.service;

import java.util.List;

import com.atc.app.dao.RunwayDAO;
import com.atc.app.dao.RunwayDAOImpl;
import com.atc.app.entity.Runway;

public class RunwayServiceImpl implements RunwayService {

	private RunwayDAO runwayDAO = new RunwayDAOImpl();
	
	@Override
	public List<Runway> listRunways() {
		return runwayDAO.listRunways();
	}

}
