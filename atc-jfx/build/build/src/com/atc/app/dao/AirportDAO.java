package com.atc.app.dao;

import java.util.List;

import com.atc.app.entity.Airport;

public interface AirportDAO {
	public List<Airport> listAirports();
}
