package com.atc.app.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.atc.app.entity.Flight;
import com.atc.app.entity.HibernateUtil;

public class FlightDAOImpl implements FlightDAO {

	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
	@Override
	public void addFlight(Flight flight) {
		Session currentSession = sessionFactory.getCurrentSession();
		try {
			currentSession.getTransaction().begin();
			currentSession.save(flight);
			currentSession.getTransaction().commit();
		}
		catch (RuntimeException e) {
			currentSession.getTransaction().rollback();
		    throw e;
		}
	}

	@Override
	public void removeFlight(Integer flightID) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateFlight(Flight flight) {
		// TODO Auto-generated method stub

	}

}
