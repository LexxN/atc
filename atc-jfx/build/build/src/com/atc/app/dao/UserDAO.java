package com.atc.app.dao;

import com.atc.app.entity.User;

public interface UserDAO {
	public User validateUser(String userName, String userPassword);

}
