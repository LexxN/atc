package com.atc.app.customs;

import com.atc.app.App;
import com.atc.app.controller.FlightController;

import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

public class FlightPath extends Path {
	private static double arcRadius = 30;
	private static final double degreeOffcet = 180;
	private static final double EPSILON = 0.00001;
	private static double tempX = 0;
	private static double tempY = 0;
	private static boolean changeCoords;
	private Color pathColor = Color.TRANSPARENT;
	
	static class SmallCurve {
		public double x1;
		public double y1;
		public double x2;
		public double y2;
		public double x3;
		public double y3;
		public double x4;
		public double y4;
		
		public SmallCurve(double x1, double y1, double x2, double y2, double x3,
				double y3, double x4, double y4) {
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
			this.x3 = x3;
			this.y3 = y3;
			this.x4 = x4;
			this.y4 = y4;
		}
	}
	
	/*
	 * Calculates straight flight paths
	 */
	public FlightPath(double startX, double startY, double newBearing, PlaneElement oPlane) {
		if(App.enableTesting) {
			pathColor = Color.WHITE;
		}
		double smallAngle, x2, y2, angle = Math.toRadians(newBearing - 90);
		if(newBearing <= 45) {
			smallAngle = Math.toRadians(newBearing);
		}
		else if(newBearing > 45 && newBearing <= 135) {
			smallAngle = Math.toRadians(Math.abs(newBearing - 90));
		}
		else if(newBearing > 135 && newBearing <= 225) {
			smallAngle = Math.toRadians(Math.abs(newBearing - 180));
		}
		else if(newBearing > 225 && newBearing <= 315) {
			smallAngle = Math.toRadians(Math.abs(newBearing - 270));
		}
		else {
			smallAngle = Math.toRadians(Math.abs(newBearing - 360));
		}

		x2 = startX + 2000 * Math.cos(angle);
		y2 = startY + 2000 * Math.sin(angle);
		
		if(x2 > FlightController.getMaxXCoord()) {
			double coordinates[] = calculateCoords(x2, y2, startY, FlightController.getMaxXCoord(), smallAngle);
			x2 = coordinates[0];
			y2 = coordinates[1];
		}
		if(y2 > FlightController.getMaxYCoord()) {
			double coordinates[] = calculateCoords(y2, x2, startX, FlightController.getMaxYCoord(), smallAngle);
			y2 = coordinates[0];
			x2 = coordinates[1];
		}
	    
	    LineTo lineTo = new LineTo(x2, y2);
	    this.getElements().add(new MoveTo(startX, startY));
	    this.getElements().add(lineTo);
	    this.setStroke(pathColor);
	    oPlane.transitionLength = Math.sqrt((startX-x2)*(startX-x2) + (startY-y2)*(startY-y2));
	    this.getStrokeDashArray().setAll(5d, 5d);
	}
	
	// TODO - needs to be improved some day
	/*
	 * Calculates landing path
	 */
	public FlightPath(double startX, double startY, double endX, double endY, PlaneElement oPlane) {
		if(App.enableTesting) {
			pathColor = Color.WHITE;
		}
		LineTo lineTo = new LineTo(endX, endY);
	    this.getElements().add(new MoveTo(startX, startY));
	    this.getElements().add(lineTo);
	    this.setStroke(pathColor);
	    oPlane.transitionLength = Math.sqrt((startX-endX)*(startX-endX) + (startY-endY)*(startY-endY));
	    this.getStrokeDashArray().setAll(5d, 5d);
	}
	
	/*
	 * Calculates turn flight paths
	 */
	public FlightPath(Integer currentBearing, Integer newBearing, Integer angle, String turn, double startX, double startY) {
		if(App.enableTesting) {
			pathColor = Color.WHITE;
		}
		this.setStroke(pathColor);
        this.getStrokeDashArray().setAll(5d, 5d);
		boolean turnType;
		changeCoords = false;
		double startAngle, endAngle;
		
		if((currentBearing < newBearing && turn.equals("noturn")) || turn.equals("r")) {
			startAngle = currentBearing;
			endAngle = startAngle + angle;
			if((startAngle < 180 && endAngle <= 270)) {
				turnType = false;
			}
			else {
				turnType = true;
			}
		}
		else {
			startAngle = currentBearing + 180;
			endAngle = startAngle - angle;
			if((startAngle <= 360)) {
				turnType = true;
			}
			else {
				turnType = false;
			}
		}
		startAngle = (startAngle - degreeOffcet)*(Math.PI / 180);
        endAngle = (endAngle - degreeOffcet)*(Math.PI / 180);
        
		final double twoPI = Math.PI * 2;
		startAngle = startAngle % twoPI;
		endAngle = endAngle % twoPI;
        
        final double piOverTwo = Math.PI / 2.0;
        final int sgn = (startAngle < endAngle) ? 1 : -1;
		
        double a1 = startAngle;
        boolean flag = false;
        for (double totalAngle = Math.min(twoPI, Math.abs(endAngle - startAngle)); totalAngle > EPSILON; ) { 
            double a2 = a1 + sgn * Math.min(totalAngle, piOverTwo);
            FlightPath.SmallCurve smallCurve = createSmallArc(a1, a2, startX, startY, turnType);
            if(!flag) {
            	this.getElements().add(new MoveTo(smallCurve.x1, smallCurve.y1));
            	flag = true;
        	}
            if(smallCurve.x4 < 0 || smallCurve.x4 > FlightController.getMaxXCoord() || smallCurve.y4 < 0 || smallCurve.y4 > FlightController.getMaxYCoord()) {
   			 	break;
   		 	}
            else {
            	CubicCurveTo smallArc = createCubicCurve(smallCurve);
                this.getElements().add(smallArc);
                totalAngle -= Math.abs(a2 - a1);
                a1 = a2;
            }
            
        }
	}
	
	public static double getArcRadius() {
		return arcRadius;
	}
	
	private double[] calculateCoords(double coord1, double coord2, double start, double max, double smallAngle) {
		double a = coord1 - max, b;
		coord1 -= a;
		b = Math.tan(smallAngle) * a;
		if(start < coord2) {
			coord2 -= b;
		}
		else {
			coord2 += b;
		}
		double coords[] = {coord1, coord2};
		return coords;
	}

	private static FlightPath.SmallCurve createSmallArc(double a1, double a2, double startX, double startY, boolean turnType) {
		double a = (a2 - a1) / 2.0;
    	
    	double x4 = arcRadius * Math.cos(a);
    	double y4 = arcRadius * Math.sin(a);
    	double x1 = x4;
    	double y1 = -y4;
    	
    	final double k = 0.5522847498;
        final double f = k * Math.tan(a);
        
    	double x2 = x1 + f * y4;
    	double y2 = y1 + f * x4;
    	double x3 = x2; 
    	double y3 = -y2;

        final double ar = a + a1;
    	final double cos_ar = Math.cos(ar);
    	final double sin_ar = Math.sin(ar);
    	
    	if((startX != (startX + arcRadius * Math.cos(a1)) || startY != (startY + arcRadius * Math.sin(a1))) && !changeCoords) {
        	tempX = Math.abs(startX - startX + arcRadius * Math.cos(a1));
        	tempY = Math.abs(startY - startY + arcRadius * Math.sin(a1));
        	if(turnType) {
        		tempX *= -1;
        		tempY *= -1;
        	}
        	changeCoords = true;
        }
    	return new FlightPath.SmallCurve(tempX + startX + arcRadius * Math.cos(a1), tempY + startY + arcRadius * Math.sin(a1), 
    			tempX + startX + x2 * cos_ar - y2 * sin_ar, tempY + startY + x2 * sin_ar + y2 * cos_ar, 
    			tempX + startX + x3 * cos_ar - y3 * sin_ar, tempY + startY + x3 * sin_ar + y3 * cos_ar, 
    			tempX + startX + arcRadius * Math.cos(a2), tempY + startY + arcRadius * Math.sin(a2)
    		   );
	}
	
	 private static CubicCurveTo createCubicCurve(FlightPath.SmallCurve oCurve) {
		 CubicCurveTo cubicTo = new CubicCurveTo();
	     cubicTo.setControlX1(oCurve.x2);
	     cubicTo.setControlY1(oCurve.y2);
	     cubicTo.setControlX2(oCurve.x3);
	     cubicTo.setControlY2(oCurve.y3);
	     cubicTo.setX(oCurve.x4);
	     cubicTo.setY(oCurve.y4);
	    
	     return cubicTo;
	}
}