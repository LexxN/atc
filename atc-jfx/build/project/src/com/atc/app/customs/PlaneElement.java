package com.atc.app.customs;

import java.util.LinkedHashSet;
import java.util.Timer;
import java.util.TimerTask;

import com.atc.app.App;
import com.atc.app.controller.FlightController;
import com.atc.app.controller.MainController;
import com.atc.app.entity.Flight;
import com.atc.app.entity.Report;
import com.atc.app.entity.Runway;

import javafx.animation.Interpolator;
import javafx.animation.PathTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.util.Duration;

public class PlaneElement extends Pane {
	
	private MainController mainController;
	
	private Label flightName;
	private Rectangle flightPointer;
	public Path currentFlightPath;
	public PathTransition pathTransition;
	private Flight oFlight;
	private Runway oRunway;
	private final static double moveSpeed = 180;
	//private final static double moveSpeed = 50;
	public double transitionLength;
	
	/*
	 * timer and refresh rate
	 */
	private Timer timer;
	private static final Integer refreshRate = 1000;
	
	/*
	 * new altitude temp variable and const climb rate
	 */
	private Integer newAltitude = 0;
	private Integer climbRate = 100;
	
	/*
	 * holds runway approaches
	 */
	public LinkedHashSet<String> runwayApproach;
	
	public Integer flightStatus;
	
	class updateDataTask extends TimerTask {
		@Override
		public void run() {
		    Platform.runLater(new Runnable() {
		    	public void run() {
		    		updateData();
		    	}
		    });
		}
	}

	public PlaneElement(Flight oFlight) {
        super();
        setPrefSize(1, 1);

        this.mainController = MainController.getMainController();
        this.setId(oFlight.getFlightName());
        this.oFlight = oFlight;
        this.flightName = new Label(oFlight.getFlightName());
        this.flightName.setFont(Font.font ("Verdana", 10));
        this.flightName.setTextFill(Color.WHITE);
        this.flightName.setLayoutY(-15);
        this.flightName.setLayoutX(-10);
        this.setStyle("-fx-cursor: hand;");
        this.flightStatus = 1;
        runwayApproach = new LinkedHashSet<String>();
        
        this.flightPointer = new Rectangle(-2, -2, 4, 4);
        this.flightPointer.setFill(Color.WHITE);
        
        getChildren().addAll(flightName, flightPointer);     

        this.setOnMouseClicked(new EventHandler<MouseEvent>(){
        	
            @Override
            public void handle(MouseEvent event) {
            	mainController.displayingDetails = true;
            	mainController.getFlightsTableView().requestFocus();
            	mainController.getFlightsTableView().getSelectionModel().select(oFlight);
            }
        });
        this.currentFlightPath = new FlightPath(oFlight.getFlightLongitude(), oFlight.getFlightLatitude(), oFlight.getFlightBearing(), this);
        this.pathTransition = new PathTransition();
        this.pathTransition.setDuration(Duration.millis(this.transitionLength * moveSpeed));
        this.pathTransition.setPath(this.currentFlightPath);
        this.pathTransition.setInterpolator(Interpolator.LINEAR);
        this.pathTransition.setNode(this);
        this.pathTransition.setOnFinished(new EventHandler<ActionEvent>(){
 
            @Override
            public void handle(ActionEvent arg0) {
            	if(flightStatus == 1) {
            		generateFlightPath(getTranslateX(), getTranslateY(), 0, 0, oFlight.getFlightBearing(), true);
            	}
            	else if(flightStatus == 0) {
            		if(oFlight.getFlightBearing() < 180) {
        				generateFlightPath(getTranslateX(), getTranslateY(), oRunway.getRunwayEndX() + FlightController.getMaxXCoord()/2, oRunway.getRunwayEndY() + FlightController.getMaxYCoord()/2, 0, false);
        			}
        			else {
        				generateFlightPath(getTranslateX(), getTranslateY(), oRunway.getRunwayStartX() + FlightController.getMaxXCoord()/2, oRunway.getRunwayStartY() + FlightController.getMaxYCoord()/2, 0, false);
        			}
            		flightStatus = 2;
            	}
            	else {
            		if(oFlight.getDestination().getAirportId() == App.airport.getAirportId()) {
            			saveReport(1);
            		}
            		else {
            			saveReport(3);
            		}
					removeFlight();
            	}
            }
        });
        
        this.timer = new Timer();
    	this.timer.schedule(new updateDataTask(), 0, refreshRate);
    }
    
	public double getMoveSpeed() {
		return moveSpeed;
	}
	
	private void generateFlightPath(double x1, double y1, double x2, double y2, double angle, boolean type) {
		mainController.getFlightsPane().getChildren().remove(this.currentFlightPath);
		if(type) {
			this.currentFlightPath = new FlightPath(x1, y1, angle, this);
		}
		else {
			this.currentFlightPath = new FlightPath(x1, y1, x2, y2, this);
		}
		this.pathTransition.setDuration(Duration.millis(this.transitionLength * moveSpeed));
		this.pathTransition.setPath(this.currentFlightPath);
		mainController.getFlightsPane().getChildren().add(this.currentFlightPath);
		this.pathTransition.play();
	}
	
	public void changeFlightBearing(Integer angle, Integer newBearing, Flight selectedFlight, String turn) {
		double startX = this.translateXProperty().get();
		double startY = this.translateYProperty().get();
		this.pathTransition.stop();
		mainController.getFlightsPane().getChildren().remove(this.currentFlightPath);
		this.currentFlightPath = new FlightPath(selectedFlight.getFlightBearing(), newBearing, angle, turn, startX, startY);
		mainController.getFlightsPane().getChildren().add(this.currentFlightPath);
		double arcPerimeterPart = (2 * Math.PI * FlightPath.getArcRadius()) / 360;
		this.flightStatus = 1;
		this.transitionLength = angle * arcPerimeterPart;
		this.pathTransition.setDuration(Duration.millis(this.transitionLength * moveSpeed));
		this.pathTransition.setPath(this.currentFlightPath);
		this.pathTransition.play();
	}
	
	public void changeFlightAltitude(Integer newAltitude) {
		this.newAltitude = newAltitude;
		if(newAltitude > this.oFlight.getFlightAltitude()) {
			climbRate = Math.abs(climbRate);
		}
		else {
			climbRate = -1 * Math.abs(climbRate);
		}
	}
	
	private void updateData() {
		if(isOutOfRange() && this.translateXProperty().get() != 0) {
			if(oFlight.getFlightWaypoint() != null) {
				boolean flag = false;
				for (Waypoint oWaypoint : FlightController.getWaypointsList()) {
					if(oWaypoint.isAtWaypoint(this.translateXProperty().get(), this.translateYProperty().get()).equals(oFlight.getFlightWaypoint())) {
						saveReport(2);
						flag = true;
						removeFlight();
					}
				}
				if(!flag) {
					saveReport(4);
					removeFlight();
				}
			}
			else {
				saveReport(4);
				removeFlight();
			}
		}
		else {
			this.oFlight.setFlightLatitude((int)this.translateYProperty().get());
			this.oFlight.setFlightLongitude((int)this.translateXProperty().get());
			int listIndex = mainController.getFlightsTableView().getSelectionModel().getSelectedIndex();
			int index = FlightController.getFlightList().indexOf(this.oFlight);
			FlightController.getFlightList().set(index, this.oFlight);
			mainController.getFlightsTableView().getSelectionModel().select(listIndex);

			if(this.newAltitude != 0) {
				int altDiff = this.newAltitude - this.oFlight.getFlightAltitude();
				if(Math.abs(altDiff) < Math.abs(climbRate)) {
					this.oFlight.setFlightAltitude(this.oFlight.getFlightAltitude() + altDiff);
					this.newAltitude = 0;
				}
				else {
					this.oFlight.setFlightAltitude(this.oFlight.getFlightAltitude() + climbRate);
				}
			}
			
			if(this.flightStatus == 0) {
				changeFlightAltitude(App.airport.getAirportElevation());
			}
		}
	}
	
	private boolean isOutOfRange() {
		if(this.translateXProperty().get() < 30 || this.translateXProperty().get() > (FlightController.getMaxXCoord() - 30) || this.translateYProperty().get() < 30 || this.translateYProperty().get() > (FlightController.getMaxYCoord() - 30)) {
			return true;
		}
		return false;
	}
	
	private void removeFlight() {
		this.timer.cancel();
		FlightController.getFlightList().remove(this.oFlight);
		if(FlightController.getFlightList().size() == 0 || this.oFlight.getFlightName().equals(mainController.getSelectedFlight().getFlightName())) {
			mainController.setSelectedFlight(null);
		}
		this.pathTransition.stop();
		FlightController.aiCount--;
		mainController.getFlightsPane().getChildren().removeAll(this, this.currentFlightPath);
	}
	
	public String landOnRunway(Runway oRunway) {
		if(this.runwayApproach.contains(oRunway.getRunwayName() + String.valueOf(this.oFlight.getFlightBearing())) && this.oFlight.getFlightAltitude() <= (App.airport.getAirportElevation() + FlightController.initialAlt)) {
			this.pathTransition.stop();
			mainController.getFlightsPane().getChildren().remove(this.currentFlightPath);
			if(this.oFlight.getFlightBearing() < 180) {
				generateFlightPath(this.getTranslateX(), this.getTranslateY(), oRunway.getRunwayStartX() + FlightController.getMaxXCoord()/2, oRunway.getRunwayStartY() + FlightController.getMaxYCoord()/2, 0, false);
			}
			else {
				generateFlightPath(this.getTranslateX(), this.getTranslateY(), oRunway.getRunwayEndX() + FlightController.getMaxXCoord()/2, oRunway.getRunwayEndY() + FlightController.getMaxYCoord()/2, 0, false);
			}
			this.oRunway = oRunway;
			this.flightStatus = 0;
			this.pathTransition.play();
			return ("Flight " + this.oFlight.getFlightName() + " is landing on runway " + oRunway.getRunwayName());
		}
		else {
			return "Invalid approach angle or altitude";
		}
	}
	
	private void saveReport(Integer recordType) {
		Report oReport = new Report(recordType, this.oFlight, App.dispatcher);
		mainController.saveRecord(oReport, this.oFlight);
	}
}