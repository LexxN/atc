package com.atc.app.customs;

import java.io.Serializable;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class Waypoint implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8165582834277938735L;
	
	/*
	 * type - which side is the border placed on
	 */
	private Integer waypointID;
	private Integer type;
	private String name;
	private Integer xCoord;
	private Integer yCoord;
	private Integer borderOffcet = 40;
	private final Integer borderLength = 30;
	private double[] borderMinCoords, borderMaxCoords;
	
	public Waypoint(Integer waypointID, Integer type, String name, Integer xCoord, Integer yCoord) {
		this.waypointID = waypointID;
		this.type = type;
		this.name = name;
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.borderMinCoords = new double[4];
		this.borderMaxCoords = new double[4];
	}
	
	public final Integer getWaypointID() {
		return waypointID;
	}
	
	public final Integer getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public String isAtWaypoint(double x, double y) {
		if(x > borderMinCoords[0] && x < borderMaxCoords[0] && y > borderMinCoords[1] && y < borderMinCoords[3] && this.type == 1) {
			System.out.println(this.type + " waypoint" + this.waypointID);
			return this.name;
		}
		else if(x > borderMinCoords[0] && x < borderMaxCoords[0] && y > borderMinCoords[3] && y < borderMinCoords[1] && this.type == 3) {
			System.out.println(this.type + " waypoint" + this.waypointID);
			return this.name;
		}
		else if(x < borderMinCoords[0] && x > borderMinCoords[2] && y > borderMinCoords[1] && y < borderMaxCoords[1] && this.type == 2) {
			System.out.println(this.type + " waypoint" + this.waypointID);
			return this.name;
		}
		else if(x > borderMinCoords[0] && x < borderMinCoords[2] && y > borderMinCoords[1] && y < borderMaxCoords[1] && this.type == 4) {
			System.out.println(this.type + " waypoint" + this.waypointID);
			return this.name;
		}
		return "";
	}
	
	public Label createWaypoing() {
		Label waypoint = new Label(this.name);
		waypoint.setLayoutX(this.xCoord);
		waypoint.setLayoutY(this.yCoord);
		waypoint.setFont(Font.font("Arial", 11));
		waypoint.setTextFill(Color.WHITE);
		waypoint.setTextAlignment(TextAlignment.CENTER);
		return waypoint;
	}
	
	public Line createBorder(boolean border) {
		Line borderLine = new Line();
		double startX, startY, endX, endY;
		borderLine.setStroke(Color.WHITE);
		borderLine.setStrokeWidth(2);
		if(!border) {
			borderOffcet = -(borderOffcet - 15);
		}
		if(this.type == 1) {
			startX = this.xCoord + borderOffcet;
			startY = this.yCoord;
			endX = this.xCoord + borderOffcet;
			endY = this.yCoord + borderLength;
		}
		else if(this.type == 2) {
			startX = this.xCoord + 15;
			startY = this.yCoord + borderOffcet;
			endX = this.xCoord - borderLength + 15;
			endY = this.yCoord + borderOffcet;
		}
		else if(this.type == 3) {
			startX = this.xCoord + borderOffcet;
			startY = this.yCoord + 20;
			endX = this.xCoord + borderOffcet;
			endY = this.yCoord - borderLength + 20;
		}
		else {
			startX = this.xCoord;
			startY = this.yCoord + borderOffcet;
			endX = this.xCoord + borderLength;
			endY = this.yCoord + borderOffcet;
		}
		
		if(!border) {
			this.borderMinCoords[0] = startX;
			this.borderMinCoords[1] = startY;
			this.borderMinCoords[2] = endX;
			this.borderMinCoords[3] = endY;
		}
		else {
			this.borderMaxCoords[0] = startX;
			this.borderMaxCoords[1] = startY;
			this.borderMaxCoords[2] = endX;
			this.borderMaxCoords[3] = endY;
		}
		
		borderLine.setStartX(startX);
		borderLine.setStartY(startY);
		borderLine.setEndX(endX);
		borderLine.setEndY(endY);
		return borderLine;
	}
}