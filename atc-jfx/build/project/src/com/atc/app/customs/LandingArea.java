package com.atc.app.customs;

import com.atc.app.App;

import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class LandingArea extends Polygon{
	
	private Integer landingAngle;
	private Color lineColor = Color.TRANSPARENT;

	public LandingArea(String runwayName, Integer landingAngle, Double[] points) {
		if(App.enableTesting) {
			lineColor = Color.RED;
		}
		this.setId(runwayName + String.valueOf(landingAngle));
		this.landingAngle = landingAngle;
		this.getPoints().addAll(points);
		this.setFill(Color.TRANSPARENT);
		this.setStroke(lineColor);
	}

	public Integer getLandingAngle() {
		return landingAngle;
	}
}
