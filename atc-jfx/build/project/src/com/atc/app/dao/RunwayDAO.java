package com.atc.app.dao;

import java.util.List;
import com.atc.app.entity.Runway;

public interface RunwayDAO {
	public List<Runway> listRunways();
}
