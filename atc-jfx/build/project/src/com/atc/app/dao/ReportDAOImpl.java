package com.atc.app.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.atc.app.App;
import com.atc.app.entity.HibernateUtil;
import com.atc.app.entity.Report;

public class ReportDAOImpl implements ReportDAO {
	
	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	@Override
	public void addReport(Report report) {
		Session currentSession = sessionFactory.getCurrentSession();
		try {
			currentSession.getTransaction().begin();
			currentSession.save(report);
			currentSession.getTransaction().commit();
		}
		catch (RuntimeException e) {
			currentSession.getTransaction().rollback();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Report> listReports() {
		Session currentSession = sessionFactory.getCurrentSession();
		List<Report> list = new ArrayList<>();
		currentSession.beginTransaction();
        list = currentSession.createQuery("FROM Report WHERE User_ID = :user_id")
        		.setParameter("user_id", App.dispatcher.getUserId())
        		.list();
        currentSession.getTransaction().commit();
        return list; 
	}

}
