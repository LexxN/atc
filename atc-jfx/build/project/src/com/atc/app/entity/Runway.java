package com.atc.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "RUNWAYS")
public class Runway implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 46657224867580186L;
	
	@Id
    @GeneratedValue(generator = "RUNWAY_SEQUENCE", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "RUNWAY_SEQUENCE", sequenceName = "RUNWAY_SEQUENCE", allocationSize = 1)
	@Column(name="RUNWAY_ID", unique = true, nullable = false)
    private Integer runwayId;
	@ManyToOne
    @JoinColumn(name="AIRPORT_ID", nullable = false)
    private Airport runwayAirport;
	@Column(name="RUNWAY_START_X", nullable = false)
    private Integer runwayStartX;
	@Column(name="RUNWAY_START_Y", nullable = false)
    private Integer runwayStartY;
	@Column(name="RUNWAY_END_X", nullable = false)
    private Integer runwayEndX;
	@Column(name="RUNWAY_END_Y", nullable = false)
    private Integer runwayEndY;
	@Column(name="RUNWAY_NAME", nullable = false)
    private String runwayName;
	@Column(name="RUNWAY_LANDING1", nullable = false)
    private Integer runwayLanding1;
	@Column(name="RUNWAY_LANDING2", nullable = false)
    private Integer runwayLanding2;
	
	public Runway() {
		super();
	}

	public Runway(Airport runwayAirport, Integer runwayStartX,
			Integer runwayStartY, Integer runwayEndX, Integer runwayEndY, String runwayName, Integer runwaLanding1, Integer runwaLanding2) {
		super();
		this.runwayAirport = runwayAirport;
		this.runwayStartX = runwayStartX;
		this.runwayStartY = runwayStartY;
		this.runwayEndX = runwayEndX;
		this.runwayEndY = runwayEndY;
		this.runwayName = runwayName;
		this.runwayLanding1 = runwaLanding1;
		this.runwayLanding2 = runwaLanding2;
	}

	public Integer getRunwayId() {
		return runwayId;
	}

	public Airport getRunwayAirport() {
		return runwayAirport;
	}

	public void setRunwayAirport(Airport runwayAirport) {
		this.runwayAirport = runwayAirport;
	}

	public Integer getRunwayStartX() {
		return runwayStartX;
	}

	public void setRunwayStartX(Integer runwayStartX) {
		this.runwayStartX = runwayStartX;
	}

	public Integer getRunwayStartY() {
		return runwayStartY;
	}

	public void setRunwayStartY(Integer runwayStartY) {
		this.runwayStartY = runwayStartY;
	}
	
	public Integer getRunwayEndX() {
		return runwayEndX;
	}

	public void setRunwayEndX(Integer runwayEndX) {
		this.runwayEndX = runwayEndX;
	}

	public Integer getRunwayEndY() {
		return runwayEndY;
	}

	public void setRunwayEndY(Integer runwayEndY) {
		this.runwayEndY = runwayEndY;
	}

	public String getRunwayName() {
		return runwayName;
	}

	public void setRunwayName(String runwayName) {
		this.runwayName = runwayName;
	}
	
	public Integer getRunwayLanding1() {
		return runwayLanding1;
	}

	public void setRunwayLanding1(Integer runwayLanding1) {
		this.runwayLanding1 = runwayLanding1;
	}

	public Integer getRunwayLanding2() {
		return runwayLanding2;
	}

	public void setRunwayLanding2(Integer runwayLanding2) {
		this.runwayLanding2 = runwayLanding2;
	}

	@Override
	public boolean equals(Object obj) {
	    if (obj instanceof Runway) {
	      Runway c = (Runway) obj;
	      if (this.runwayName.equalsIgnoreCase(c.getRunwayName()))
	         return true;
	    }
	    return false;
	}
}
