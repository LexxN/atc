package com.atc.app.controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.atc.app.App;
import com.atc.app.customs.Direction;
import com.atc.app.customs.LandingArea;
import com.atc.app.customs.PlaneElement;
import com.atc.app.customs.Waypoint;
import com.atc.app.entity.Flight;
import com.atc.app.entity.Report;
import com.atc.app.entity.Runway;
import com.atc.app.screenframework.ControlledScreen;
import com.atc.app.screenframework.ScreensController;
import com.atc.app.service.FlightService;
import com.atc.app.service.FlightServiceImpl;
import com.atc.app.service.ReportService;
import com.atc.app.service.ReportServiceImpl;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
	
public class MainController implements Initializable, ControlledScreen {
	
	ScreensController screenController;
	private static MainController mainController;
	private Flight selectedFlight;
	private FlightService flightService;
	private ReportService reportService;
	protected ObservableList<Report> reportsList;
	protected ReportController reportController;
	
	@FXML protected GridPane mainGrid;
	@FXML private TableView<Flight> flightsTableView;
	@FXML protected AnchorPane flightsPane;
	@FXML protected TextField orderText;
	@FXML protected Button transmitButton;
	@FXML private TableColumn<Flight, String> flightNameCol;
	@FXML private TableColumn<Flight, String> flightCoordinatesCol;
	@FXML private TableColumn<Flight, String> flightAltCol;
	@FXML private TableColumn<Flight, String> flightFuelCol;
	@FXML protected Label warningMessageLabel;
	@FXML private Label descrLabel1;
	@FXML private Label descrLabel2;
	@FXML private Label descrLabel3;
	@FXML private Label descrLabel4;
	@FXML private Label descrLabel5;
	@FXML private Label descrLabel6;
	@FXML private Label descrLabel7;
	@FXML private Label descrLabel8;
	@FXML private Label valueLabel1;
	@FXML private Label valueLabel2;
	@FXML private Label valueLabel3;
	@FXML private Label valueLabel4;
	@FXML private Label valueLabel5;
	@FXML private Label valueLabel6;
	@FXML private Label valueLabel7;
	@FXML private Label valueLabel8;
	
	public boolean displayingDetails = true;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {		
		selectedFlight = new Flight();
		this.flightService = new FlightServiceImpl();
		this.reportService = new ReportServiceImpl();
		flightsTableView.setPlaceholder(new Label("updating..."));
		flightsTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Flight>() {

			@Override
			public void changed(ObservableValue<? extends Flight> observable, Flight oldValue, Flight newValue) {
				if(newValue != null) {
					if(displayingDetails) {
						displayDetails(newValue, true, null);
					}
					else {
						displayDetails(newValue, false, null);
					}
				}
			}
		});
		
		flightNameCol.setCellValueFactory(
        		cellData -> new SimpleStringProperty(cellData.getValue().getFlightName() != null ? cellData.getValue().getFlightName() : "null"));
		flightCoordinatesCol.setCellValueFactory(
        		cellData -> flightIsFlying(cellData.getValue()) ? cellData.getValue().getCoordsProperty() : new SimpleStringProperty("awaits for runway"));
		flightAltCol.setCellValueFactory(
        		cellData -> new SimpleStringProperty(cellData.getValue().getFlightAltitude() != null ? cellData.getValue().getFlightAltitude() + "ft" : "null"));
		flightFuelCol.setCellValueFactory(
        		cellData -> new SimpleStringProperty(cellData.getValue().getFlightFuel() != null ? cellData.getValue().getFlightFuel() + "m" : "null"));
	}
	
	@Override
	public void setScreenParent(ScreensController screenParent) {
		screenController = screenParent;
	}
	
	@Override
	public void setScreenController(Object object) {
		MainController.mainController = (MainController) object;	
	}

	public static MainController getMainController() {
		return mainController;
	}
	
	public Pane getFlightsPane() {
		return this.flightsPane;
	}
	
	public TableView<Flight> getFlightsTableView() {
		return this.flightsTableView;
	}
	
	public void setSelectedFlight(Flight selectedFlight) {
		this.selectedFlight = selectedFlight;
	}

	public Flight getSelectedFlight() {
		return selectedFlight;
	}
	
	public void displayAirport(List<Runway> airportRunwayList, Integer maxXCoord, Integer maxYCoord) {
		for (Runway oRunway : airportRunwayList) {
			double startX = oRunway.getRunwayStartX() + maxXCoord/2;
			double startY = oRunway.getRunwayStartY() + maxYCoord/2;
			double endX = oRunway.getRunwayEndX() + maxXCoord/2;
			double endY = oRunway.getRunwayEndY() + maxYCoord/2;
			Text name = new Text(startX - 15, startY + 4, oRunway.getRunwayName());
			name.setFont(Font.font ("Verdana", 10));
			name.setFill(Color.WHITE);
			Line runway = new Line(startX, startY, endX, endY);
			runway.setStyle("-fx-cursor: hand;");
			runway.setStrokeWidth(5);
			runway.setStroke(Color.WHITE);
			runway.setOnMouseClicked(new EventHandler<MouseEvent>(){
	        	
	            @Override
	            public void handle(MouseEvent event) {
	            	mainController.displayDetails(null, true, oRunway);
	            }
	        });
			mainController.flightsPane.getChildren().addAll(name, runway);
			
			Double[] points1 = new Double[]{
					(double) (maxXCoord - 30), (double) (oRunway.getRunwayEndY() + maxYCoord/2 - 20),
					(double) (oRunway.getRunwayEndX() + maxXCoord/2 + 50), (double) (oRunway.getRunwayEndY() + maxYCoord/2 - 10),
				    (double) (oRunway.getRunwayEndX() + maxXCoord/2 + 50), (double) (oRunway.getRunwayEndY() + maxYCoord/2 + 10),
				    (double) (maxXCoord - 30), (double) (oRunway.getRunwayEndY() + maxYCoord/2 + 20)};
			Polygon area1 = new LandingArea(oRunway.getRunwayName(), oRunway.getRunwayLanding2(), points1);
			
			Double[] points2 = new Double[]{
					(double) (30), (double) (oRunway.getRunwayStartY() + maxYCoord/2 - 20),
					(double) (oRunway.getRunwayStartX() + maxXCoord/2 - 50), (double) (oRunway.getRunwayStartY() + maxYCoord/2 - 10),
				    (double) (oRunway.getRunwayStartX() + maxXCoord/2 - 50), (double) (oRunway.getRunwayStartY() + maxYCoord/2 + 10),
				    (double) (30), (double) (oRunway.getRunwayStartY() + maxYCoord/2 + 20)};
			Polygon area2 = new LandingArea(oRunway.getRunwayName(), oRunway.getRunwayLanding1(), points2);
			
			mainController.flightsPane.getChildren().addAll(area1, area2);
		}
		
	}
	
	public void displayOutline(List<Direction> directionsList, List<Waypoint> waypointsList, Integer maxXCoord, Integer maxYCoord) {
		for (Direction oDirection :  directionsList) {
			mainController.flightsPane.getChildren().add(oDirection.createLabel());
		}
		
		for (Waypoint oWaypoint :  waypointsList) {
			mainController.flightsPane.getChildren().add(oWaypoint.createWaypoing());
			mainController.flightsPane.getChildren().add(oWaypoint.createBorder(true));
			mainController.flightsPane.getChildren().add(oWaypoint.createBorder(false));
		}
		
		Line border = new Line(30, 30, maxXCoord-30, 30);
		border.setStroke(Color.RED);
		border.getStrokeDashArray().setAll(5d, 5d);
		
		Line border2 = new Line(maxXCoord-30, 30, maxXCoord-30, maxYCoord-30);
		border2.setStroke(Color.RED);
		border2.getStrokeDashArray().setAll(5d, 5d);
		
		Line border3 = new Line(30, maxYCoord-30, maxXCoord-30, maxYCoord-30);
		border3.setStroke(Color.RED);
		border3.getStrokeDashArray().setAll(5d, 5d);
		
		Line border4 = new Line(30, 30, 30, maxYCoord-30);
		border4.setStroke(Color.RED);
		border4.getStrokeDashArray().setAll(5d, 5d);
		
		mainController.flightsPane.getChildren().addAll(border, border2, border3, border4);
	}

	public void displayDetails(Flight oFlight, boolean flag, Runway oRunway) {
		if(oFlight != null && flag) {
			displayingDetails = true;
			changeDescription(new String[]{"Flight", "Coordinates", "Bearing", "Altitude", "Origin", "Destination", "Fuel", "Plane"});
			selectedFlight = oFlight;
			valueLabel1.setText(selectedFlight.getFlightName());
			if(flightIsFlying(oFlight)) {
				valueLabel2.textProperty().bind(oFlight.getCoordsProperty());
				valueLabel3.setText(String.valueOf(oFlight.getFlightBearing()) + "\u00b0");
			}
			else {
				valueLabel2.textProperty().bind(new SimpleStringProperty("awaits for runway"));
				valueLabel3.setText("awaits for runway");
			}
			valueLabel4.textProperty().bind(oFlight.getAltitudeProperty());
			valueLabel5.setText(oFlight.getOrigin().getAirportName().split("/")[0]);
			if(oFlight.getFlightWaypoint() == null) {
				valueLabel6.setText(oFlight.getDestination().getAirportName().split("/")[0]);
			}
			else {
				valueLabel6.setText(oFlight.getFlightWaypoint() + " to " + oFlight.getDestination().getAirportName().split("/")[0]);
			}
			valueLabel7.setText(String.valueOf(oFlight.getFlightFuel()) + "mins");
			valueLabel8.setText(oFlight.getPlaneType().getPlaneName());
		}
		else if(oRunway != null){
			displayingDetails = false;
			changeDescription(new String[]{"Runway", "Landing angles", "Airport", "Coordinates", "Elevation", "Landing alt.", "", ""});
			valueLabel1.setText(oRunway.getRunwayName());
			valueLabel2.textProperty().bind(new SimpleStringProperty(oRunway.getRunwayLanding1() + "\u00b0 & " + oRunway.getRunwayLanding2() + "\u00b0"));
			valueLabel3.setText(App.airport.getAirportName());
			valueLabel4.textProperty().bind(new SimpleStringProperty(App.airport.getAirportLongitude() + "x" + App.airport.getAirportLatitude()));
			valueLabel5.setText(App.airport.getAirportElevation() + "ft");
			valueLabel6.setText(((App.airport.getAirportElevation()/100) * 100 + FlightController.landingMaxAlt) + "ft");
			valueLabel7.setText("");
			valueLabel8.setText("");
		}
	}
	
	private void changeDescription(String[] data) {
		descrLabel1.setText(data[0]);
		descrLabel2.setText(data[1]);
		descrLabel3.setText(data[2]);
		descrLabel4.setText(data[3]);
		descrLabel5.setText(data[4]);
		descrLabel6.setText(data[5]);
		descrLabel7.setText(data[6]);
		descrLabel8.setText(data[7]);
	}
	
	@FXML
	private void transmitButtonAction(ActionEvent event) {
		if (orderText.getText().contains(" ")) {
			String[] orders = orderText.getText().split(" ");
			switch (orders[0]) {
			case "b":
				validateTurn(orders);
				break;
			case "c":
				if(flightIsFlying(selectedFlight)) {
					changeAltitude(Integer.valueOf(orders[1]));
				}
				else {
					warningMessageLabel.setText("Invalid command");
				}
				break;
			case "w":
				//TODO - autopilot to waypoints
				//System.out.println("Autopilot set to " + orders[1]);
				break;
			case "r":
				takeOffRunway(orders[1]);
				break;
			case "l":
				landOnRunway(orders[1]);
				break;
			default :
				warningMessageLabel.setText("Invalid command");
				break;						
			}
		}
		else {
			warningMessageLabel.setText("Invalid command");
		}
		orderText.clear();
	}
	
	private void validateTurn(String[] orders) {
		if(flightIsFlying(selectedFlight)) {
			if(Integer.valueOf(orders[1]) >= 0 && Integer.valueOf(orders[1]) <= 360) {
				/*Integer angle = Integer.valueOf(orders[1]) - selectedFlight.getFlightBearing();
				angle += (angle > 180) ? -360 : (angle < -180) ? 360 : 0;*/
				Integer angle = Math.abs(Integer.valueOf(orders[1]) - selectedFlight.getFlightBearing());
				
				if(orders.length == 3 && angle != 0) {
					if((orders[2].equals("l") || orders[2].equals("r")) && angle == 180) {
						changeBearing(Integer.valueOf(orders[1]), orders[2], angle);
					}
					else {
						warningMessageLabel.setText("Invalid turn command.");
					}
				}
				else if(orders.length == 2 && angle != 0 && angle != 180) {
					changeBearing(Integer.valueOf(orders[1]), "noturn", angle);
				}
				else {
					warningMessageLabel.setText("Invalid command.");
				}
			}
			else {
				warningMessageLabel.setText("Bearing out of range");
			}
		}
		else {
			warningMessageLabel.setText("Invalid command");
		}
	}
	
	private void changeBearing(Integer newBearing, String turn, Integer angle) {
		PlaneElement oPlane = (PlaneElement) flightsPane.lookup("#" + selectedFlight.getFlightName());		
		oPlane.flightStatus = 1;
		warningMessageLabel.setText("Flight " + selectedFlight.getFlightName() + " is changing bearing to " + newBearing + "\u00b0");
		oPlane.changeFlightBearing(angle, newBearing, selectedFlight, turn);
		if(newBearing == 360) {
			newBearing = 0;
		}
		selectedFlight.setFlightBearing(newBearing);
	}
	
	private void changeAltitude(Integer newAltitude) {
		if(newAltitude >= ((App.airport.getAirportElevation()/100) * 100 + FlightController.landingMaxAlt) && newAltitude <= FlightController.maxAlt && newAltitude != selectedFlight.getFlightAltitude()) {
			PlaneElement oPlane = (PlaneElement) flightsPane.lookup("#" + selectedFlight.getFlightName());			
			warningMessageLabel.setText(null);
			oPlane.changeFlightAltitude(newAltitude);
		}
		else {
			warningMessageLabel.setText("Invalid altitude");
		}
	}
	
	private void takeOffRunway(String runway) {
		Runway oRunway = new Runway();
		oRunway.setRunwayName(runway);
		if(FlightController.airportRunwayList.contains(oRunway)) {
			oRunway = FlightController.airportRunwayList.get(FlightController.airportRunwayList.indexOf(oRunway));
			if(!flightIsFlying(selectedFlight)) {
				int index = FlightController.flightList.indexOf(selectedFlight);
				selectedFlight.setFlightLatitude(oRunway.getRunwayStartY() + FlightController.getMaxYCoord()/2);
				selectedFlight.setFlightLongitude(oRunway.getRunwayStartX() + FlightController.getMaxXCoord()/2);
				selectedFlight.setFlightBearing(90);
				FlightController.flightList.set(index, selectedFlight);
				FlightController.displayFlight(selectedFlight, true);
				warningMessageLabel.setText("Flight " + selectedFlight.getFlightName() + " is taking off runway " + oRunway.getRunwayName());				
			}
			else {
				warningMessageLabel.setText("Invalid command");
			}
		}
		else {
			warningMessageLabel.setText("Runway doesn't exist.");
		}
	}
	
	private void landOnRunway(String runway) {
		Runway oRunway = new Runway();
		oRunway.setRunwayName(runway);
		if(FlightController.airportRunwayList.contains(oRunway)) {
			oRunway = FlightController.airportRunwayList.get(FlightController.airportRunwayList.indexOf(oRunway));
			if(flightIsFlying(selectedFlight)) {
				PlaneElement oPlane = (PlaneElement) flightsPane.lookup("#" + selectedFlight.getFlightName());			
				warningMessageLabel.setText(null);
				warningMessageLabel.setText(oPlane.landOnRunway(oRunway));
			}
			else {
				warningMessageLabel.setText("Invalid command");
			}
		}
		else {
			warningMessageLabel.setText("Runway doesn't exist.");
		}
	}
	
	private boolean flightIsFlying(Flight oFlight) {
		if(oFlight.getFlightLatitude() != 0 && oFlight.getFlightLongitude() != 0) {
			return true;
		}
		else {
			return false;
		}
	}
	

	public void saveRecord(Report oReport, Flight oFlight) {
		flightService.addFlight(oFlight);
		reportService.addReport(oReport);
	}
	
	@FXML
	public void commandsMenuAction(ActionEvent event) {
		Stage stage = displayPopUp("view/Commands.fxml", "Commands", false);
		stage.showAndWait();
	}
	
	@FXML
	public void reportsMenuAction(ActionEvent event) {
		Stage stage = displayPopUp("view/Reports.fxml", "Reports", true);
		if(stage != null) {
			reportsList = FXCollections.observableList((List<Report>) reportService.listReports());			
			reportController.getReportsTableView().setItems(reportsList);
			stage.showAndWait();
		}
	}
	
	private Stage displayPopUp(String file, String title, boolean type) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource(file));
			AnchorPane page = (AnchorPane) loader.load();
			Stage dialogStage = new Stage();
			dialogStage.setTitle(title);
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(App.primaryStage);
			Scene scene = new Scene(page);
			scene.getStylesheets().add("/styles/main.css");
			dialogStage.setScene(scene);
			if(type) {
				reportController = loader.getController();
			}
			return dialogStage;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@FXML
	public void exitMenuAction(ActionEvent event) {
		System.exit(0);
		Platform.exit();
	}
}
