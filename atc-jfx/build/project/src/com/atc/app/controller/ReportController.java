package com.atc.app.controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import com.atc.app.entity.Report;

public class ReportController implements Initializable {
	
	private ArrayList<String> reportStatuses = new ArrayList<String>();
	
	@FXML private TableView<Report> reportsTableView;
	@FXML private TableColumn<Report, String> reportFlightCol;
	@FXML private TableColumn<Report, String> reportOriginCol;
	@FXML private TableColumn<Report, String> reportDestinationCol;
	@FXML private TableColumn<Report, String> reportAltitudeCol;
	@FXML private TableColumn<Report, String> reportTypeCol;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		reportStatuses.add("Landed successfully");
		reportStatuses.add("Left our airspace successfully");
		reportStatuses.add("Left our airspace without permission");
		reportStatuses.add("Did not reach destination");
		reportStatuses.add("TODO");
		reportStatuses.add("TODO");
		
		reportFlightCol.setCellValueFactory(
        		cellData -> new SimpleStringProperty(cellData.getValue().getFlight() != null ? cellData.getValue().getFlight().getFlightName() : "null"));
		reportOriginCol.setCellValueFactory(
        		cellData -> new SimpleStringProperty(cellData.getValue().getFlight().getOrigin() != null ? cellData.getValue().getFlight().getOrigin().getAirportName().split("/")[0] : "null"));
		reportDestinationCol.setCellValueFactory(
        		cellData -> new SimpleStringProperty(cellData.getValue().getFlight().getDestination() != null ? cellData.getValue().getFlight().getDestination().getAirportName().split("/")[0] : "null"));
		reportAltitudeCol.setCellValueFactory(
        		cellData -> new SimpleStringProperty(cellData.getValue().getFlight() != null ? cellData.getValue().getFlight().getFlightAltitude() + "ft" : "null"));
		reportTypeCol.setCellValueFactory(
				cellData -> new SimpleStringProperty(cellData.getValue() != null ? reportStatuses.get(cellData.getValue().getReportType() - 1) : "null"));
	}

	public TableView<Report> getReportsTableView() {
		return reportsTableView;
	}
}
