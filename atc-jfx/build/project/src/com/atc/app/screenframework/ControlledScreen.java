package com.atc.app.screenframework;


public interface ControlledScreen {
    
    //This method will allow the injection of the Parent ScreenPane
    public void setScreenParent(ScreensController screenPage);
    
    public void setScreenController(Object object);
}
