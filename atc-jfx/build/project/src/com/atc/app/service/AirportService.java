package com.atc.app.service;

import java.util.List;

import com.atc.app.entity.Airport;

public interface AirportService {
	public List<Airport> listAirports();
}
