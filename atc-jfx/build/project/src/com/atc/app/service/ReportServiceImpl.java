package com.atc.app.service;

import java.util.List;

import com.atc.app.dao.ReportDAO;
import com.atc.app.dao.ReportDAOImpl;
import com.atc.app.entity.Report;

public class ReportServiceImpl implements ReportService{

	private ReportDAO reportDAO = new ReportDAOImpl();
	
	@Override
	public void addReport(Report report) {
		reportDAO.addReport(report);
	}

	@Override
	public List<Report> listReports() {
		return reportDAO.listReports();
	}

}
