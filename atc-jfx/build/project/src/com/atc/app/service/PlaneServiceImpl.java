package com.atc.app.service;

import java.util.List;

import com.atc.app.dao.PlaneDAO;
import com.atc.app.dao.PlaneDAOImpl;
import com.atc.app.entity.Plane;

public class PlaneServiceImpl implements PlaneService {
	
	private PlaneDAO planeDAO = new PlaneDAOImpl();

	@Override
	public List<Plane> listPlaneTypes() {
		return planeDAO.listPlaneTypes();
	}

}
