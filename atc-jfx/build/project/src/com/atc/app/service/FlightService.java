package com.atc.app.service;

import com.atc.app.entity.Flight;

public interface FlightService {
	public void addFlight(Flight flight);
	public void removeFlight(Integer flightID);
	public void updateFlight(Flight flight);
}
