package com.atc.app.service;

import com.atc.app.dao.FlightDAO;
import com.atc.app.dao.FlightDAOImpl;
import com.atc.app.entity.Flight;

public class FlightServiceImpl implements FlightService {

	private FlightDAO flightDAO = new FlightDAOImpl();
	
	@Override
	public void addFlight(Flight flight) {
		flightDAO.addFlight(flight);
	}

	@Override
	public void removeFlight(Integer flightID) {
		flightDAO.removeFlight(flightID);
	}

	@Override
	public void updateFlight(Flight flight) {
		flightDAO.updateFlight(flight);
	}

}
